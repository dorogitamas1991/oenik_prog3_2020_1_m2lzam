/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Konyvajanlo;

import java.io.Serializable;
import java.util.Random;
import java.util.stream.IntStream;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dorogi tamas
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class KonyvVasarlas implements Serializable{
    

    private String title;

    private String iro;
    
    private int ar;
    
   
    public KonyvVasarlas()
    {
        
    }
    

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the iro
     */
    public String getIro() {
        return iro;
    }

    /**
     * @param iro the iro to set
     */
    public void setIro(String iro) {
        this.iro = iro;
    }

    /**
     * @return the ar
     */
    public int getAr() {
        return ar;
    }

    /**
     * @param ar the ar to set
     */
    public void setAr(int ar) {
        this.ar = ar;
    }

   
    
}
