﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookStore
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;
    using BookStore.Data;
    using BookStore.Logic.Interfaces;
    using BookStore.Logic.Services;

    /// <summary>
    /// Program class.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The menu method of the program.
        /// </summary>
        private static void Menu()
        {
            var services = BusinessLogic.CreateBL();
            Console.WriteLine();
            Console.WriteLine("1. Adatok lekérése (Read)");
            Console.WriteLine("2. Új rekord felvétele (Create)");
            Console.WriteLine("3. Rekord törlése (Delete)");
            Console.WriteLine("4. Frissítés (Update)");
            Console.WriteLine("5. Webes lekérés");
            Console.WriteLine("6. Lekérdezések");
            Console.WriteLine("7. Exit");
            int menuszam = int.Parse(Console.ReadLine());
            Console.Clear();
            switch (menuszam)
            {
                case 1:
                    Console.WriteLine("Milyen adatokat szeretnél lekérni?\n");
                    Console.WriteLine("1. Írók");
                    Console.WriteLine("2. Boltok");
                    Console.WriteLine("3. Könyvek");
                    Console.WriteLine("4. Vissza");
                    menuszam = int.Parse(Console.ReadLine());
                    Console.Clear();
                    switch (menuszam)
                    {
                        case 1:
                            IroKiir(services);
                            break;

                        case 2:
                            BoltKiir(services);
                            break;

                        case 3:
                            KonyvekKiir(services);
                            break;
                    }

                    break;
                case 2:
                    Console.WriteLine("Hová szeretnél új rekordot felvenni? (1. Új író; 2. Új bolt; 3. Új könyv");
                    menuszam = int.Parse(Console.ReadLine());
                    switch (menuszam)
                    {
                        case 1:
                            services.CreateAuthor(CreateAuthor());
                            Console.WriteLine("Új író sikeresen hozzáadva az adatbázishoz. NYOMJ EGY ENTERT A FOLYTATÁSHOZ!");
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 2:
                            services.CreateStore(CreateStore());
                            Console.WriteLine("Új bolt sikeresen hozzáadva az adatbázishoz. NYOMJ EGY ENTERT A FOLYTATÁSHOZ!");
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 3:
                            CreateBook(services);

                            Console.WriteLine("Új könyv sikeresen hozzáadva az adatbázishoz. NYOMJ EGY ENTERT A FOLYTATÁSHOZ!");
                            Console.ReadLine();
                            Console.Clear();
                            break;
                    }

                    break;
                case 3:
                    int torlendo;
                    Console.WriteLine("Honnan szeretnél rekordot törölni?? (1. Könyvek; 2. Boltok; 3. Írók)");
                    menuszam = int.Parse(Console.ReadLine());
                    switch (menuszam)
                    {
                        case 1:
                            Console.WriteLine("Melyik könyvet szeretnéd törölni? (Írd be a számát!)");
                            Console.WriteLine();
                            var bookForDelete = services.GetBooks();

                            foreach (var item in bookForDelete)
                            {
                                Console.Write("{0}. {1}, \n", item.bookId, item.bookTitle);
                            }

                            torlendo = int.Parse(Console.ReadLine());
                            services.DeleteBookPieces(torlendo);
                            services.DeleteBook(torlendo);
                            Console.Clear();
                            Console.WriteLine("Könyv törölve.");
                            break;
                        case 2:
                            Console.WriteLine("Melyik boltot szeretnéd törölni? (Írd be a számát!)");
                            Console.WriteLine();
                            var storeForDelete = services.GetStores();

                            foreach (var item in storeForDelete)
                            {
                                Console.Write("{0}. {1}, {2}\n", item.storeId, item.storename, item.storeAddress);
                            }

                            torlendo = int.Parse(Console.ReadLine());
                            services.DeleteStorePieces(torlendo);
                            services.DeleteStore(torlendo);
                            Console.Clear();
                            Console.WriteLine("Bolt törölve.");
                            break;
                        case 3:
                            Console.WriteLine("Melyik írót szeretnéd törölni? Ezzel a könyveit is törlöd. (Írd be a számát!)");
                            Console.WriteLine();
                            var authorForDelete = services.GetAuthors();

                            foreach (var item in authorForDelete)
                            {
                                Console.Write("{0}. {1}, \n", item.authorId, item.authorName);
                            }

                            torlendo = int.Parse(Console.ReadLine());
                            var booksForDelete = services.GetBooks();
                            var selectedForDelete = new List<books>();
                            foreach (var item in booksForDelete)
                            {
                                if (item.writerId == torlendo)
                                {
                                    selectedForDelete.Add(item);
                                }
                            }

                            foreach (var item in selectedForDelete)
                            {
                                services.DeleteBookPieces(item.bookId);
                                services.DeleteBook(item.bookId);
                            }

                            services.DeleteAuthor(torlendo);
                            Console.Clear();
                            Console.WriteLine("Író törölve."); break;
                    }

                    break;
                case 4:
                    Console.WriteLine("Melyik táblát szeretnéd frissíteni?(1. Írók, 2. Könyvek, 3. Boltok ");
                    menuszam = int.Parse(Console.ReadLine());
                    Console.WriteLine();
                    switch (menuszam)
                    {
                        case 1:
                            Console.WriteLine("Melyik rekordot frissítenéd?");
                            IroKiir(services);
                            int recordnumber = int.Parse(Console.ReadLine());
                            var updateauthor = services.GetAuthorsById(recordnumber);
                            Console.WriteLine("Név:");
                            var name = Console.ReadLine();
                            Console.WriteLine("Nemzetiség:");
                            var orszag = Console.ReadLine();
                            Console.WriteLine("Kor:");
                            int age = int.Parse(Console.ReadLine());
                            Console.WriteLine("1. Férfi / 2. Nő");
                            int gender = int.Parse(Console.ReadLine());
                            bool whatGender = gender == 1 ? false : true;
                            Console.WriteLine("1. Él / 2. Elhunyt");
                            int alive = int.Parse(Console.ReadLine());
                            bool isAlive = alive == 1 ? true : false;
                            updateauthor.authorName = name;
                            updateauthor.nationality = orszag;
                            updateauthor.age = age;
                            updateauthor.gender = whatGender;
                            updateauthor.alive = isAlive;
                            services.UpdateAuthor(updateauthor);
                            Console.WriteLine("Író frissítve");
                            break;
                        case 2:
                            Console.WriteLine("Melyik rekordot frissítenéd?");
                            KonyvekKiir(services);
                            int recordnumberbook = int.Parse(Console.ReadLine());
                            var updatebook = services.GetBooksById(recordnumberbook);
                            Console.WriteLine("Cím:");
                            var title = Console.ReadLine();
                            Console.WriteLine("Műfaj:");
                            var type = Console.ReadLine();
                            Console.WriteLine("Író Id-ja az adatbázisban:");
                            Console.WriteLine();
                            var authorIdForbookCreate = services.GetAuthors();

                            foreach (var item in authorIdForbookCreate)
                            {
                                Console.Write("{0}. {1}, \n", item.authorId, item.authorName);
                            }

                            int writerId = int.Parse(Console.ReadLine());
                            Console.WriteLine("Oldalszám:");
                            int pages = int.Parse(Console.ReadLine());
                            Console.WriteLine("ISBN számsor:");
                            string isbn = Console.ReadLine();
                            updatebook.bookTitle = title;
                            updatebook.booktype = type;
                            updatebook.writerId = writerId;
                            updatebook.pages = pages;
                            updatebook.isbn = isbn;
                            services.UpdateBook(updatebook);
                            Console.WriteLine("Könyv frissítve");
                            break;
                        case 3:
                            Console.WriteLine("Melyik rekordot frissítenéd?");
                            BoltKiir(services);
                            int recordnumberstore = int.Parse(Console.ReadLine());
                            var updatestore = services.GetStoreById(recordnumberstore);
                            Console.WriteLine("Név:");
                            var storename = Console.ReadLine();
                            Console.WriteLine("Cím:");
                            var storeaddress = Console.ReadLine();
                            Console.WriteLine("Megjegyzés:");
                            var megjegyzes = Console.ReadLine();
                            Console.WriteLine("Értékelés (1-5)");
                            int rating = int.Parse(Console.ReadLine());
                            updatestore.storename = storename;
                            updatestore.storeAddress = storeaddress;
                            updatestore.megjegyzés = megjegyzes;
                            updatestore.rating = rating;
                            services.UpdateStore(updatestore);
                            Console.WriteLine("Bolt frissítve");
                            break;
                    }

                    break;
                case 5:
                    Console.WriteLine("Add meg az író nevét");
                    string addiro = Console.ReadLine();
                    Console.WriteLine("Add meg a könyv címét");
                    string addtitle = Console.ReadLine();
                    string url = $"http://localhost:8080/BookStore.JavaApp/BookStoreServlet?iro={addiro}&title={addtitle}&elkuld=Elk%C3%BCld%C3%A9s";
                    XDocument xdoc = XDocument.Load(url);
                    Console.WriteLine();
                    foreach (var item in xdoc.Descendants("konyvVasarlas"))
                    {
                        Console.WriteLine("A legjobb talált ár a {0}: {1} című könyvre:", item.Element("iro").Value, item.Element("title").Value);
                        Console.WriteLine(item.Element("ar").Value + " Ft");
                    }

                    break;
                case 6:
                    Console.WriteLine("Válasszon egy lekérdezést");
                    Console.WriteLine("1. Megvásárolható könyvei a megadott írónak");
                    Console.WriteLine("2. Egy adott könyv írójának lekérdezése");
                    Console.WriteLine("3. Leltár (Hány db adott könyv van egy boltban?)");
                    Console.WriteLine("4. Darabszám az összes boltban műfajra leosztva");
                    Console.WriteLine("5. Darabszám az összes boltban címre leosztva");
                    Console.WriteLine("6. Írók korainak átlaga a könyv műfajok szerint");
                    int beker = int.Parse(Console.ReadLine());
                    switch (beker)
                    {
                        case 1:
                            Console.WriteLine("Listázza a bekért író könyveit (Kérek egy írót)");
                            string ironev = Console.ReadLine();
                            Console.Clear();
                            Console.WriteLine("{0} elérhető könyvei:", ironev);
                            List<string> konyvek = services.BookSearchBasedOnName(ironev);
                            foreach (var item in konyvek)
                            {
                                Console.WriteLine(item);
                            }

                            break;
                        case 2:
                            Console.WriteLine("Melyik könyv íróját szeretné megtudni?");
                            string title = Console.ReadLine();
                            Console.Clear();
                            Console.Write("{0} írója: ", title);
                            string iro = services.AuthorSearchBasedOnTitle(title);
                            Console.Write(iro);
                            Console.WriteLine();
                            break;
                        case 3:
                            Console.WriteLine("Adja meg a könyv címét, amit ki szeretne keresni");
                            string title2 = Console.ReadLine();
                            Console.Clear();
                            Console.WriteLine(title2);
                            Console.WriteLine("Bolt neve \t\t Bolt címe \t\t db");
                            var piecesQuery = services.PiecesInWhichStore(title2);
                            foreach (var item in piecesQuery)
                            {
                                Console.WriteLine(item.StoreName + "\t\t" + item.StoreAddress + "\t\t" + item.Pieces);
                            }

                            break;
                        case 4:
                            Console.Clear();
                            var groupedPiecesByGenre = services.SumOfTypePieces();
                            foreach (var item in groupedPiecesByGenre)
                            {
                                Console.WriteLine(item.Type + ": " + item.Pieces + " db");
                            }

                            break;
                        case 5:
                            Console.Clear();
                            var groupedByTitlesPieces = services.SumOfBooksByTitle();
                            foreach (var item in groupedByTitlesPieces)
                            {
                                Console.WriteLine(item.Title + ": " + item.Pieces + " db");
                            }

                            break;
                        case 6:
                            Console.Clear();
                            var genreAgeAvgs = services.AVGofWritersAgeByGenre();
                            foreach (var item in genreAgeAvgs)
                            {
                                Console.WriteLine(item.Genre + " írók korainak átlaga: " + (int)item.Age);
                            }

                            break;
                    }

                    break;
                case 7:
                    Environment.Exit(0);
                    break;
            }
        }

        /// <summary>
        /// Main function.
        /// </summary>
        /// <param name="args">args.</param>
        private static void Main(string[] args)
        {
            while (true)
            {
                Menu();
            }
        }

        /// <summary>
        /// Writes out the result of GetBooks().
        /// </summary>
        /// <param name="services">Expectins a BusinessLogic object.</param>
        private static void KonyvekKiir(ILogic services)
        {
            var allbook = services.GetBooks();

            foreach (var item in allbook)
            {
                Console.WriteLine("{0}. {1}, {2}, Oldalszám: {3}, {4}", item.bookId, item.bookTitle, item.booktype, item.pages, item.isbn);
            }
        }

        /// <summary>
        /// Writes out the result of GetStores().
        /// </summary>
        /// <param name="services">Expectins a BusinessLogic object.</param>
        private static void BoltKiir(ILogic services)
        {
            var allstore = services.GetStores();

            foreach (var item in allstore)
            {
                Console.Write("{0}. {1}, {2}, {3}, {4}*\n", item.storeId, item.storename, item.storeAddress, item.megjegyzés, item.rating);
            }
        }

        /// <summary>
        /// Writes out the result of GetAuthors().
        /// </summary>
        /// <param name="services">Expectins a BusinessLogic object.</param>
        private static void IroKiir(ILogic services)
        {
            var allauthor = services.GetAuthors();

            foreach (var item in allauthor)
            {
                string authorgender = item.gender == false ? "Férfi" : "Nő";
                Console.Write("{0}. {1}, {2}, Kor: {3}, {4}\n", item.authorId, item.authorName, item.nationality, item.age, authorgender);
            }
        }

        /// <summary>
        /// Creates an Author.
        /// </summary>
        /// <returns>Returns with the created author.</returns>
        private static authors CreateAuthor()
        {
            Console.WriteLine("Név:");
            var name = Console.ReadLine();
            Console.WriteLine("Nemzetiség:");
            var orszag = Console.ReadLine();
            Console.WriteLine("Kor:");
            int age = int.Parse(Console.ReadLine());
            Console.WriteLine("1. Férfi / 2. Nő");
            int gender = int.Parse(Console.ReadLine());
            bool whatGender = gender == 1 ? false : true;
            Console.WriteLine("1. Él / 2. Elhunyt");
            int alive = int.Parse(Console.ReadLine());
            bool isAlive = alive == 1 ? true : false;
            var createAuthor = new authors()
            {
                authorName = name,
                nationality = orszag,
                age = age,
                gender = whatGender,
                alive = isAlive,
            };
            return createAuthor;
        }

        /// <summary>
        /// Creates a Book.
        /// </summary>
        private static void CreateBook(ILogic bl)
        {
            Console.WriteLine("Cím:");
            var title = Console.ReadLine();
            Console.WriteLine("Műfaj:");
            var type = Console.ReadLine();
            Console.WriteLine("Író Id-ja az adatbázisban:");
            Console.WriteLine();
            var authorIdForbookCreate = bl.GetAuthors();

            foreach (var item in authorIdForbookCreate)
            {
                Console.Write("{0}. {1}, \n", item.authorId, item.authorName);
            }

            int writerId = int.Parse(Console.ReadLine());
            Console.WriteLine("Oldalszám:");
            int pages = int.Parse(Console.ReadLine());
            Console.WriteLine("ISBN számsor:");
            string isbn = Console.ReadLine();
            var createBook = new books()
            {
                bookTitle = title,
                booktype = type,
                writerId = writerId,
                pages = pages,
                isbn = isbn,
            };

            bl.CreateBook(createBook);
            PiecesCreate(createBook, bl);
        }

        /// <summary>
        /// Inserts a created book into pieces table.
        /// </summary>
        /// <param name="book">Book that needs to be inserted</param>
        /// <param name="businessLogic">The budsinesslogic object</param>
        private static void PiecesCreate(books book, ILogic businessLogic)
        {
            Console.WriteLine("Melyik boltban elérhető ez a könyv?");
            var storelist = businessLogic.GetStores();
            foreach (var item in storelist)
            {
                Console.WriteLine("{0}. {1}, Cím: {2}", item.storeId, item.storename, item.storeAddress);
            }

            int storenumber = int.Parse(Console.ReadLine());
            Console.WriteLine("Hány darab van raktáron?");
            int db = int.Parse(Console.ReadLine());

            var createPiece = new pieces()
            {
                booksId = book.bookId,
                storeId = storenumber,
                pieces1 = db,
            };

            businessLogic.CreatePiece(createPiece);
        }

        /// <summary>
        /// Creates a store.
        /// </summary>
        /// <returns>Returns with the created store.</returns>
        private static store CreateStore()
        {
            Console.WriteLine("Név:");
            var storename = Console.ReadLine();
            Console.WriteLine("Cím:");
            var storeaddress = Console.ReadLine();
            Console.WriteLine("Megjegyzés:");
            var megjegyzes = Console.ReadLine();
            Console.WriteLine("Értékelés (1-5)");
            int rating = int.Parse(Console.ReadLine());
            var createStore = new store()
            {
                storeAddress = storeaddress,
                storename = storename,
                megjegyzés = megjegyzes,
                rating = rating,
            };
            return createStore;
        }
    }
}
