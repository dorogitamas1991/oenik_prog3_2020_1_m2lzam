var namespace_book_store_1_1_logic =
[
    [ "Interfaces", "namespace_book_store_1_1_logic_1_1_interfaces.html", "namespace_book_store_1_1_logic_1_1_interfaces" ],
    [ "ServiceModels", "namespace_book_store_1_1_logic_1_1_service_models.html", "namespace_book_store_1_1_logic_1_1_service_models" ],
    [ "Services", "namespace_book_store_1_1_logic_1_1_services.html", "namespace_book_store_1_1_logic_1_1_services" ],
    [ "Tests", "namespace_book_store_1_1_logic_1_1_tests.html", "namespace_book_store_1_1_logic_1_1_tests" ]
];