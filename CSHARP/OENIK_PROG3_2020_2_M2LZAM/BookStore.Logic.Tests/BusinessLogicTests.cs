﻿// <copyright file="BusinessLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookStore.Logic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using BookStore.Data;
    using BookStore.Logic.ServiceModels;
    using BookStore.Logic.Services;
    using BookStore.Repository.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// BusinessLogic test class.
    /// </summary>
    [TestFixture]
    public class BusinessLogicTests
    {
        /// <summary>
        /// Test of The Read all book method.
        /// </summary>
        /// <param name="booklist">Test book list.</param>
        /// <param name="authorlist">Test author list.</param>
        /// <param name="storelist">Test Store list.</param>
        /// <param name="pieceslist">Test pieces list.</param>
        /// <param name="booksize">Count of the book list.</param>
        /// <param name="authorsize">Count of the author list.</param>
        /// <param name="storesize">Count of the store list.</param>
        /// <param name="piecessize">Count of the pieces list.</param>
        [TestCaseSource(nameof(GetAll))]
        public void ReadBookTest(List<books> booklist, List<authors> authorlist, List<store> storelist, List<pieces> pieceslist, int booksize, int authorsize, int storesize, int piecessize)
        {
            // arrange
            var mockedBookRepository = new Mock<IRepository<books>>();
            var mockedAuthorRepository = new Mock<IRepository<authors>>();
            var mockedStoreRepository = new Mock<IRepository<store>>();
            var mockedPiecesRepository = new Mock<IRepositoryPieces>();

            mockedBookRepository.Setup(m => m.ReadAll()).Returns(booklist.AsQueryable);

            var service = new BusinessLogic(mockedBookRepository.Object, mockedAuthorRepository.Object, mockedStoreRepository.Object, mockedPiecesRepository.Object);

            // act
            var allbook = service.GetBooks();

            // assert
            Assert.That(allbook, Is.Not.Null);
            Assert.That(allbook.Count, Is.EqualTo(booksize));
            mockedBookRepository.Verify(m => m.ReadAll(), Times.Once);
        }

        /// <summary>
        /// Test of The Read all author method.
        /// </summary>
        /// <param name="booklist">Test book list.</param>
        /// <param name="authorlist">Test author list.</param>
        /// <param name="storelist">Test Store list.</param>
        /// <param name="pieceslist">Test pieces list.</param>
        /// <param name="booksize">Count of the book list.</param>
        /// <param name="authorsize">Count of the author list.</param>
        /// <param name="storesize">Count of the store list.</param>
        /// <param name="piecessize">Count of the pieces list.</param>
        [TestCaseSource(nameof(GetAll))]
        public void ReadAuthorTest(List<books> booklist, List<authors> authorlist, List<store> storelist, List<pieces> pieceslist, int booksize, int authorsize, int storesize, int piecessize)
        {
            // arrange
            var mockedBookRepository = new Mock<IRepository<books>>();
            var mockedAuthorRepository = new Mock<IRepository<authors>>();
            var mockedStoreRepository = new Mock<IRepository<store>>();
            var mockedPiecesRepository = new Mock<IRepositoryPieces>();

            mockedAuthorRepository.Setup(m => m.ReadAll()).Returns(authorlist.AsQueryable);

            var service = new BusinessLogic(mockedBookRepository.Object, mockedAuthorRepository.Object, mockedStoreRepository.Object, mockedPiecesRepository.Object);

            // act
            var allauthor = service.GetAuthors();

            // assert
            Assert.That(allauthor, Is.Not.Null);
            Assert.That(allauthor.Count, Is.EqualTo(authorsize));
            mockedAuthorRepository.Verify(m => m.ReadAll(), Times.Once);
        }

        /// <summary>
        /// Test of The Read all store method.
        /// </summary>
        /// <param name="booklist">Test book list.</param>
        /// <param name="authorlist">Test author list.</param>
        /// <param name="storelist">Test Store list.</param>
        /// <param name="pieceslist">Test pieces list.</param>
        /// <param name="booksize">Count of the book list.</param>
        /// <param name="authorsize">Count of the author list.</param>
        /// <param name="storesize">Count of the store list.</param>
        /// <param name="piecessize">Count of the pieces list.</param>
        [TestCaseSource(nameof(GetAll))]
        public void ReadStoreTest(List<books> booklist, List<authors> authorlist, List<store> storelist, List<pieces> pieceslist, int booksize, int authorsize, int storesize, int piecessize)
        {
            // arrange
            var mockedBookRepository = new Mock<IRepository<books>>();
            var mockedAuthorRepository = new Mock<IRepository<authors>>();
            var mockedStoreRepository = new Mock<IRepository<store>>();
            var mockedPiecesRepository = new Mock<IRepositoryPieces>();

            mockedStoreRepository.Setup(m => m.ReadAll()).Returns(storelist.AsQueryable);

            var service = new BusinessLogic(mockedBookRepository.Object, mockedAuthorRepository.Object, mockedStoreRepository.Object, mockedPiecesRepository.Object);

            // act
            var allstore = service.GetStores();

            // assert
            Assert.That(allstore, Is.Not.Null);
            Assert.That(allstore.Count, Is.EqualTo(storesize));
            mockedStoreRepository.Verify(m => m.ReadAll(), Times.Once);
        }

        /// <summary>
        /// Test of the Create Book method.
        /// </summary>
        [Test]
        public void CreateBookTest()
        {
            // arrange
            var mockedBookRepository = new Mock<IRepository<books>>();
            var mockedAuthorRepository = new Mock<IRepository<authors>>();
            var mockedStoreRepository = new Mock<IRepository<store>>();
            var mockedPiecesRepository = new Mock<IRepositoryPieces>();

            mockedBookRepository.Setup(m => m.Create(It.IsAny<books>())).Verifiable();

            var service = new BusinessLogic(mockedBookRepository.Object, mockedAuthorRepository.Object, mockedStoreRepository.Object, mockedPiecesRepository.Object);

            // act
            service.CreateBook(It.IsAny<books>());

            // assert
            mockedBookRepository.Verify(m => m.Create(It.IsAny<books>()), Times.Once);
        }

        /// <summary>
        /// Test of the Create Author method.
        /// </summary>
        [Test]
        public void CreateAuthorTest()
        {
            // arrange
            var mockedBookRepository = new Mock<IRepository<books>>();
            var mockedAuthorRepository = new Mock<IRepository<authors>>();
            var mockedStoreRepository = new Mock<IRepository<store>>();
            var mockedPiecesRepository = new Mock<IRepositoryPieces>();

            mockedAuthorRepository.Setup(m => m.Create(It.IsAny<authors>())).Verifiable();

            var service = new BusinessLogic(mockedBookRepository.Object, mockedAuthorRepository.Object, mockedStoreRepository.Object, mockedPiecesRepository.Object);

            // act
            service.CreateAuthor(It.IsAny<authors>());

            // assert
            mockedAuthorRepository.Verify(m => m.Create(It.IsAny<authors>()), Times.Once);
        }

        /// <summary>
        /// Test of the Create Store method.
        /// </summary>
        [Test]
        public void CreateStoreTest()
        {
            // arrange
            var mockedBookRepository = new Mock<IRepository<books>>();
            var mockedAuthorRepository = new Mock<IRepository<authors>>();
            var mockedStoreRepository = new Mock<IRepository<store>>();
            var mockedPiecesRepository = new Mock<IRepositoryPieces>();

            mockedStoreRepository.Setup(m => m.Create(It.IsAny<store>())).Verifiable();

            var service = new BusinessLogic(mockedBookRepository.Object, mockedAuthorRepository.Object, mockedStoreRepository.Object, mockedPiecesRepository.Object);

            // act
            service.CreateStore(It.IsAny<store>());

            // assert
            mockedStoreRepository.Verify(m => m.Create(It.IsAny<store>()), Times.Once);
        }

        /// <summary>
        /// Test of the Update Book method.
        /// </summary>
        [Test]
        public void UpdateBookTest()
        {
            // arrange
            var mockedBookRepository = new Mock<IRepository<books>>();
            var mockedAuthorRepository = new Mock<IRepository<authors>>();
            var mockedStoreRepository = new Mock<IRepository<store>>();
            var mockedPiecesRepository = new Mock<IRepositoryPieces>();
            var book = new books();

            mockedBookRepository.Setup(m => m.Update(It.IsAny<books>())).Verifiable();

            var service = new BusinessLogic(mockedBookRepository.Object, mockedAuthorRepository.Object, mockedStoreRepository.Object, mockedPiecesRepository.Object);

            // act
            service.UpdateBook(book);

            // assert
            mockedBookRepository.Verify(m => m.Update(It.IsAny<books>()), Times.Once);
        }

        /// <summary>
        /// Test of the Update Author method.
        /// </summary>
        [Test]
        public void UpdateAuthorTest()
        {
            // arrange
            var mockedBookRepository = new Mock<IRepository<books>>();
            var mockedAuthorRepository = new Mock<IRepository<authors>>();
            var mockedStoreRepository = new Mock<IRepository<store>>();
            var mockedPiecesRepository = new Mock<IRepositoryPieces>();
            var author = new authors();

            mockedAuthorRepository.Setup(m => m.Update(It.IsAny<authors>())).Verifiable();

            var service = new BusinessLogic(mockedBookRepository.Object, mockedAuthorRepository.Object, mockedStoreRepository.Object, mockedPiecesRepository.Object);

            // act
            service.UpdateAuthor(author);

            // assert
            mockedAuthorRepository.Verify(m => m.Update(It.IsAny<authors>()), Times.Once);
        }

        /// <summary>
        /// Test of the Update Store method.
        /// </summary>
        [Test]
        public void UpdateStoreTest()
        {
            // arrange
            var mockedBookRepository = new Mock<IRepository<books>>();
            var mockedAuthorRepository = new Mock<IRepository<authors>>();
            var mockedStoreRepository = new Mock<IRepository<store>>();
            var mockedPiecesRepository = new Mock<IRepositoryPieces>();
            var store = new store();

            mockedStoreRepository.Setup(m => m.Update(It.IsAny<store>())).Verifiable();

            var service = new BusinessLogic(mockedBookRepository.Object, mockedAuthorRepository.Object, mockedStoreRepository.Object, mockedPiecesRepository.Object);

            // act
            service.UpdateStore(store);

            // assert
            mockedStoreRepository.Verify(m => m.Update(It.IsAny<store>()), Times.Once);
        }

        /// <summary>
        ///  Test of the Delete Book method.
        /// </summary>
        [Test]
        public void DeleteBookTest()
        {
            // arrange
            var mockedBookRepository = new Mock<IRepository<books>>();
            var mockedAuthorRepository = new Mock<IRepository<authors>>();
            var mockedStoreRepository = new Mock<IRepository<store>>();
            var mockedPiecesRepository = new Mock<IRepositoryPieces>();
            var book = new books() { bookId = 1, };

            mockedBookRepository.Setup(m => m.Delete(It.IsAny<decimal>())).Verifiable();

            var service = new BusinessLogic(mockedBookRepository.Object, mockedAuthorRepository.Object, mockedStoreRepository.Object, mockedPiecesRepository.Object);

            // act
            service.DeleteBook(book.bookId);

            // assert
            mockedBookRepository.Verify(m => m.Delete(book.bookId), Times.Once);
        }

        /// <summary>
        ///  Test of the Delete Author method.
        /// </summary>
        [Test]
        public void DeleteAuthorTest()
        {
            // arrange
            var mockedBookRepository = new Mock<IRepository<books>>();
            var mockedAuthorRepository = new Mock<IRepository<authors>>();
            var mockedStoreRepository = new Mock<IRepository<store>>();
            var mockedPiecesRepository = new Mock<IRepositoryPieces>();
            var author = new authors() { authorId = 1, };

            mockedAuthorRepository.Setup(m => m.Delete(It.IsAny<decimal>())).Verifiable();

            var service = new BusinessLogic(mockedBookRepository.Object, mockedAuthorRepository.Object, mockedStoreRepository.Object, mockedPiecesRepository.Object);

            // act
            service.DeleteAuthor(author.authorId);

            // assert
            mockedAuthorRepository.Verify(m => m.Delete(author.authorId), Times.Once);
        }

        /// <summary>
        ///  Test of the Delete Author method.
        /// </summary>
        [Test]
        public void DeleteStoreTest()
        {
            // arrange
            var mockedBookRepository = new Mock<IRepository<books>>();
            var mockedAuthorRepository = new Mock<IRepository<authors>>();
            var mockedStoreRepository = new Mock<IRepository<store>>();
            var mockedPiecesRepository = new Mock<IRepositoryPieces>();
            var store = new store() { storeId = 1, };

            mockedStoreRepository.Setup(m => m.Delete(It.IsAny<decimal>())).Verifiable();

            var service = new BusinessLogic(mockedBookRepository.Object, mockedAuthorRepository.Object, mockedStoreRepository.Object, mockedPiecesRepository.Object);

            // act
            service.DeleteStore(store.storeId);

            // assert
            mockedStoreRepository.Verify(m => m.Delete(store.storeId), Times.Once);
        }

        /// <summary>
        /// Test of the query which returns with the list of how many pieces of books we have all together grouped by the titles.
        /// </summary>
        /// <param name="booklist">Test book list.</param>
        /// <param name="authorlist">Test author list.</param>
        /// <param name="storelist">Test store list.</param>
        /// <param name="pieceslist">Test pieces list.</param>
        /// <param name="booksize">Size of the test book list.</param>
        /// <param name="authorsize">Size of the test author list.</param>
        /// <param name="storesize">Size of the test store list.</param>
        /// <param name="piecessize">Size of the test pieces list.</param>
        [TestCaseSource(nameof(GetAll))]
        public void BookPieceSumByTitleTest(List<books> booklist, List<authors> authorlist, List<store> storelist, List<pieces> pieceslist, int booksize, int authorsize, int storesize, int piecessize)
        {
            // arrange
            var mockedBookRepository = new Mock<IRepository<books>>();
            var mockedAuthorRepository = new Mock<IRepository<authors>>();
            var mockedStoreRepository = new Mock<IRepository<store>>();
            var mockedPiecesRepository = new Mock<IRepositoryPieces>();

            mockedBookRepository.Setup(m => m.ReadAll()).Returns(booklist.AsQueryable);
            mockedPiecesRepository.Setup(m => m.ReadAll()).Returns(pieceslist.AsQueryable);

            var service = new BusinessLogic(mockedBookRepository.Object, mockedAuthorRepository.Object, mockedStoreRepository.Object, mockedPiecesRepository.Object);

            // act
            List<BookPiecesSumByTitle> result = service.SumOfBooksByTitle();

            // assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(4));
            Assert.That(result[0].Title, Is.EqualTo("A gyűrűk Ura"));
            Assert.That(result[0].Pieces, Is.EqualTo(50));
            mockedBookRepository.Verify(m => m.ReadAll(), Times.Once);
            mockedPiecesRepository.Verify(m => m.ReadAll(), Times.Once);
        }

        /// <summary>
        /// Test of the query which returns with the list of how many pieces of a certain book we have in which store.
        /// </summary>
        /// <param name="booklist">Test book list.</param>
        /// <param name="authorlist">Test author list.</param>
        /// <param name="storelist">Test store list.</param>
        /// <param name="pieceslist">Test pieces list.</param>
        /// <param name="booksize">Size of the test book list.</param>
        /// <param name="authorsize">Size of the test author list.</param>
        /// <param name="storesize">Size of the test store list.</param>
        /// <param name="piecessize">Size of the test pieces list.</param>
        [TestCaseSource(nameof(GetAll))]
        public void PiecesInWhichStoreTest(List<books> booklist, List<authors> authorlist, List<store> storelist, List<pieces> pieceslist, int booksize, int authorsize, int storesize, int piecessize)
        {
            // arrange
            var mockedBookRepository = new Mock<IRepository<books>>();
            var mockedAuthorRepository = new Mock<IRepository<authors>>();
            var mockedStoreRepository = new Mock<IRepository<store>>();
            var mockedPiecesRepository = new Mock<IRepositoryPieces>();

            mockedBookRepository.Setup(m => m.ReadAll()).Returns(booklist.AsQueryable);
            mockedPiecesRepository.Setup(m => m.ReadAll()).Returns(pieceslist.AsQueryable);
            mockedStoreRepository.Setup(m => m.ReadAll()).Returns(storelist.AsQueryable);

            var service = new BusinessLogic(mockedBookRepository.Object, mockedAuthorRepository.Object, mockedStoreRepository.Object, mockedPiecesRepository.Object);

            // act
            var result = service.PiecesInWhichStore("lovecraft összes");

            // assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(2));
            Assert.That(result[0].StoreName, Is.EqualTo("Libri"));
            Assert.That(result[0].Pieces, Is.EqualTo(10));
            Assert.That(result[1].StoreName, Is.EqualTo("Alexandra"));
            Assert.That(result[1].Pieces, Is.EqualTo(20));
            mockedBookRepository.Verify(m => m.ReadAll(), Times.Once);
            mockedPiecesRepository.Verify(m => m.ReadAll(), Times.Once);
        }

        /// <summary>
        /// Test of the AverageOfWritersAgeInGenre metthod.
        /// </summary>
        /// <param name="booklist">Test book list.</param>
        /// <param name="authorlist">Test author list.</param>
        /// <param name="storelist">Test store list.</param>
        /// <param name="pieceslist">Test pieces list.</param>
        /// <param name="booksize">Size of the test book list.</param>
        /// <param name="authorsize">Size of the test author list.</param>
        /// <param name="storesize">Size of the test store list.</param>
        /// <param name="piecessize">Size of the test pieces list.</param>
        [TestCaseSource(nameof(GetAll))]
        public void AverageAgesTest(List<books> booklist, List<authors> authorlist, List<store> storelist, List<pieces> pieceslist, int booksize, int authorsize, int storesize, int piecessize)
        {
            // arrange
            var mockedBookRepository = new Mock<IRepository<books>>();
            var mockedAuthorRepository = new Mock<IRepository<authors>>();
            var mockedStoreRepository = new Mock<IRepository<store>>();
            var mockedPiecesRepository = new Mock<IRepositoryPieces>();

            mockedBookRepository.Setup(m => m.ReadAll()).Returns(booklist.AsQueryable);
            mockedAuthorRepository.Setup(m => m.ReadAll()).Returns(authorlist.AsQueryable);

            var service = new BusinessLogic(mockedBookRepository.Object, mockedAuthorRepository.Object, mockedStoreRepository.Object, mockedPiecesRepository.Object);

            // act
            var result = service.AVGofWritersAgeByGenre();

            // assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(3));
            Assert.That(result[0].Genre, Is.EqualTo("fantasy"));
            Assert.That(result[0].Age, Is.EqualTo((82 + 70) / 2));
            mockedBookRepository.Verify(m => m.ReadAll(), Times.Once);
            mockedAuthorRepository.Verify(m => m.ReadAll(), Times.Once);
        }

        /// <summary>
        /// Creating a test "database".
        /// </summary>
        /// <returns>TestCaseData.</returns>
        private static IEnumerable<TestCaseData> GetAll()
        {
            var result = new List<TestCaseData>();

            var books = new List<books>();
            books.Add(NewBook(1, "A gyűrűk Ura", "fantasy", 1));
            books.Add(NewBook(2, "Hóember", "skandináv krimi", 2));
            books.Add(NewBook(3, "Trónok Harca", "fantasy", 3));
            books.Add(NewBook(4, "Lovecraft összes", "horror", 4));

            var authors = new List<authors>();
            authors.Add(NewAuthor(1, "J.R.R. Tolkien", 82, "angol", false));
            authors.Add(NewAuthor(2, "Jo Nesbo", 56, "norvég", true));
            authors.Add(NewAuthor(3, "George R.R. Martin", 70, "amerikai", true));
            authors.Add(NewAuthor(4, "H.P. Lovecraft", 63, "amerikai", false));

            var stores = new List<store>();
            stores.Add(NewStore(1, "Libri", "Radnóti u.4", "Vasárnap is nyitva", 5));
            stores.Add(NewStore(2, "Alexandra", "Mátyás Király u. 3", "Eladót felveszünk", 4));

            var pieces = new List<pieces>();
            pieces.Add(NewPieces(1, 1, 1, 30));
            pieces.Add(NewPieces(2, 1, 2, 20));
            pieces.Add(NewPieces(3, 2, 1, 40));
            pieces.Add(NewPieces(4, 3, 2, 20));
            pieces.Add(NewPieces(5, 4, 1, 10));
            pieces.Add(NewPieces(6, 4, 2, 20));

            result.Add(new TestCaseData(books, authors, stores, pieces, 4, 4, 2, 6));
            return result;
        }

        private static books NewBook(int id, string title, string booktype, int authorid)
        {
            var newbook = new books()
            {
                bookId = id,
                bookTitle = title,
                booktype = booktype,
                writerId = authorid,
            };
            return newbook;
        }

        private static authors NewAuthor(int id, string nev, int age, string nationality, bool alive)
        {
            var newauthor = new authors()
            {
                authorId = id,
                authorName = nev,
                age = age,
                nationality = nationality,
                alive = alive,
            };
            return newauthor;
        }

        private static store NewStore(int id, string storeName, string address, string megjegyzes, int rating)
        {
            var newStore = new store()
            {
                storeId = id,
                storeAddress = address,
                megjegyzés = megjegyzes,
                rating = rating,
                storename = storeName,
            };
            return newStore;
        }

        private static pieces NewPieces(int id, int bookid, int storeId, int pieces)
        {
            var newPiece = new pieces()
            {
                piecesID = id,
                booksId = bookid,
                storeId = storeId,
                pieces1 = pieces,
            };
            return newPiece;
        }

        private static IEnumerable<TestCaseData> CreateMethods()
        {
            var result = new List<TestCaseData>();

            var book = NewBook(4, "Lovecraft összes", "horror", 4);

            var author = NewAuthor(1, "J.R.R. Tolkien", 82, "angol", false);

            var store = NewStore(1, "Libri", "Radnóti u.4", "Vasárnap is nyitva", 5);

            result.Add(new TestCaseData(book, author, store));
            return result;
        }
    }
}
