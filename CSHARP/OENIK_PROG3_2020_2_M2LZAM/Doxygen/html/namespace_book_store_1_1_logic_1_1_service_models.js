var namespace_book_store_1_1_logic_1_1_service_models =
[
    [ "AverageOfWritersAgeInGenreModel", "class_book_store_1_1_logic_1_1_service_models_1_1_average_of_writers_age_in_genre_model.html", "class_book_store_1_1_logic_1_1_service_models_1_1_average_of_writers_age_in_genre_model" ],
    [ "BookPiecesSumByTitle", "class_book_store_1_1_logic_1_1_service_models_1_1_book_pieces_sum_by_title.html", "class_book_store_1_1_logic_1_1_service_models_1_1_book_pieces_sum_by_title" ],
    [ "PiecesInWhichStoreModel", "class_book_store_1_1_logic_1_1_service_models_1_1_pieces_in_which_store_model.html", "class_book_store_1_1_logic_1_1_service_models_1_1_pieces_in_which_store_model" ],
    [ "SumTypePiecesModel", "class_book_store_1_1_logic_1_1_service_models_1_1_sum_type_pieces_model.html", "class_book_store_1_1_logic_1_1_service_models_1_1_sum_type_pieces_model" ]
];