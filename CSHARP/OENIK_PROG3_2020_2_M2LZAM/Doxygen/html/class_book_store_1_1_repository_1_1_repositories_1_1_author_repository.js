var class_book_store_1_1_repository_1_1_repositories_1_1_author_repository =
[
    [ "AuthorRepository", "class_book_store_1_1_repository_1_1_repositories_1_1_author_repository.html#aefb07b6f266be17f6fc0323b78fe1f64", null ],
    [ "Create", "class_book_store_1_1_repository_1_1_repositories_1_1_author_repository.html#a25f0bd0f3ddc62961fb0439a2558af04", null ],
    [ "Delete", "class_book_store_1_1_repository_1_1_repositories_1_1_author_repository.html#ad890fe8b860f2a5159b2fa771edb747f", null ],
    [ "GetById", "class_book_store_1_1_repository_1_1_repositories_1_1_author_repository.html#af6b114126a241353c8f230a2f7f9b7f5", null ],
    [ "ReadAll", "class_book_store_1_1_repository_1_1_repositories_1_1_author_repository.html#a0bd9bbcf5a7e866f35e7d63edcdf3032", null ],
    [ "Update", "class_book_store_1_1_repository_1_1_repositories_1_1_author_repository.html#ac05f0ae78fe8a92faf95bc026595a34e", null ]
];