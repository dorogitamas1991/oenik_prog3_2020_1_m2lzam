var interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository =
[
    [ "Create", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html#afdfc9e6bbf965ffa153ecffcff8995e4", null ],
    [ "Delete", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html#a5e82c7069cbbf4828830c1eef53a62ac", null ],
    [ "GetById", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html#a80556aa942cc27513180497f8c560108", null ],
    [ "ReadAll", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html#ae73c7a468cc5f99d2e1482f37cdde89c", null ],
    [ "Update", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html#a9b2329631eba29a2dfd48410a77b3655", null ]
];