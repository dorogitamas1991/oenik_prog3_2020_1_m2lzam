﻿// <copyright file="AuthorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookStore.Repository.Repositories
{
    using System;
    using System.Linq;
    using BookStore.Data;
    using BookStore.Repository.Interfaces;

    /// <summary>
    /// Repository for authors, which implements the IRepository with T as Authors.
    /// </summary>
    public class AuthorRepository : IRepository<authors>
    {
        /// <summary>
        /// The context of the database.
        /// </summary>
        private BookDBEntities dbcontext;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorRepository"/> class.
        /// </summary>
        /// <param name="dbcontext">Waiting for a created database context.</param>
        public AuthorRepository(BookDBEntities dbcontext)
        {
            this.dbcontext = dbcontext;
        }

        /// <summary>
        /// Adds a new author to the dbcontext.
        /// </summary>
        /// <param name="author">The author needs to be added.</param>
        public void Create(authors author)
        {
            var result = this.dbcontext.authors.Add(author);
            this.dbcontext.SaveChanges();
        }

        /// <summary>
        /// Deletes an author based on its id.
        /// </summary>
        /// <param name="id">The id of the author which needs to be deleted.</param>
        public void Delete(decimal id)
        {
            this.dbcontext.authors.Remove(this.GetById(id));
            this.dbcontext.SaveChanges();
        }

        /// <summary>
        /// Updates an author.
        /// </summary>
        /// <param name="data">The updatable author.</param>
        public void Update(authors data)
        {
            var entry = this.dbcontext.Entry(this.GetById(data.authorId));
            entry.CurrentValues.SetValues(data);
            this.dbcontext.SaveChanges();
        }

        /// <summary>
        /// Get an author based on its id.
        /// </summary>
        /// <param name="id">The id of the author.</param>
        /// <returns>The wanted author.</returns>
        public authors GetById(decimal id)
        {
            var result = this.dbcontext.authors.SingleOrDefault(x => x.authorId == id);
            if (result == null)
            {
                throw new ApplicationException("Nincsen az id-nek megfelelő író az adatbázisban");
            }

            return result;
        }

        /// <summary>
        /// Gives back the content of the author table.
        /// </summary>
        /// <returns>The queryable author table.</returns>
        public IQueryable<authors> ReadAll()
        {
            return this.dbcontext.authors;
        }
    }
}
