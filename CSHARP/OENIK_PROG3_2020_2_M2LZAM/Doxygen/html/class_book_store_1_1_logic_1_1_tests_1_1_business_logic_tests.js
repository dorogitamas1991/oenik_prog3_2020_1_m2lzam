var class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests =
[
    [ "AverageAgesTest", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#a977a482eb0fb4703545f9ef16651d5c9", null ],
    [ "BookPieceSumByTitleTest", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#a2d79e4b94e59a146ffda73aa78747d93", null ],
    [ "CreateAuthorTest", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#a3ae1a94f7e34fd5990080f21cc00bfbe", null ],
    [ "CreateBookTest", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#a26e5d51a7c24db0925aa63fe447c2e68", null ],
    [ "CreateStoreTest", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#ad23d8dc759c7898cd4b1e685eb7a98fb", null ],
    [ "DeleteAuthorTest", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#accbe41f62cbcdda2d07cf3703ad29897", null ],
    [ "DeleteBookTest", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#a3c59fac28f88856dce733d661978e61c", null ],
    [ "DeleteStoreTest", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#ab314f7d1771840cb94b5ea11c2d1ab86", null ],
    [ "PiecesInWhichStoreTest", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#a381c0d80f604299a987a2b39cffce6a7", null ],
    [ "ReadAuthorTest", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#afb417b40c7f0b8fd0c5f46ec90ab27ec", null ],
    [ "ReadBookTest", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#a65b8fec4552792f976a11023fcd68eaa", null ],
    [ "ReadStoreTest", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#a9f105be7bdaae30bb5445f0f1a958018", null ],
    [ "UpdateAuthorTest", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#a482e4d52591d2b2b7517a5d8d13dd0d1", null ],
    [ "UpdateBookTest", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#a542f46bd7c59291ff3be948d44545039", null ],
    [ "UpdateStoreTest", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#acdeeeab73816ed8384fa44ac0b3f25a2", null ]
];