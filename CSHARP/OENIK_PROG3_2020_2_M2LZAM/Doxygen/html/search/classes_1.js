var searchData=
[
  ['bookdbentities_92',['BookDBEntities',['../class_book_store_1_1_data_1_1_book_d_b_entities.html',1,'BookStore::Data']]],
  ['bookpiecessumbytitle_93',['BookPiecesSumByTitle',['../class_book_store_1_1_logic_1_1_service_models_1_1_book_pieces_sum_by_title.html',1,'BookStore::Logic::ServiceModels']]],
  ['books_94',['books',['../class_book_store_1_1_data_1_1books.html',1,'BookStore::Data']]],
  ['booksrepository_95',['BooksRepository',['../class_book_store_1_1_repository_1_1_books_repository.html',1,'BookStore::Repository']]],
  ['businesslogic_96',['BusinessLogic',['../class_book_store_1_1_logic_1_1_services_1_1_business_logic.html',1,'BookStore::Logic::Services']]],
  ['businesslogictests_97',['BusinessLogicTests',['../class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html',1,'BookStore::Logic::Tests']]]
];
