﻿// <copyright file="PiecesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookStore.Repository.Repositories
{
    using System;
    using System.Linq;
    using BookStore.Data;
    using BookStore.Repository.Interfaces;

    /// <summary>
    /// Repository for pieces, which implements the IPiecesRepository.
    /// </summary>
    public class PiecesRepository : IRepositoryPieces
    {
        private BookDBEntities dbcontext;

        /// <summary>
        /// Initializes a new instance of the <see cref="PiecesRepository"/> class.
        /// </summary>
        /// <param name="dbcontext">The database context.</param>
        public PiecesRepository(BookDBEntities dbcontext)
        {
            this.dbcontext = dbcontext;
        }

        /// <summary>
        /// Gives a new pieces record to the pieces table.
        /// </summary>
        /// <param name="data">the pieces which is wanted to be added.</param>
        public void Create(pieces data)
        {
            var result = this.dbcontext.pieces.Add(data);
            this.dbcontext.SaveChanges();
        }

        /// <summary>
        /// Deletes a record based on its book id.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        public void DeleteBook(decimal id)
        {
            this.dbcontext.pieces.RemoveRange(this.BookGetById(id));
            this.dbcontext.SaveChanges();
        }

        /// <summary>
        /// Deletes the record based on a store id.
        /// </summary>
        /// <param name="id">The id of the store.</param>
        public void DeleteStore(decimal id)
        {
            this.dbcontext.pieces.RemoveRange(this.StoreGetById(id));
            this.dbcontext.SaveChanges();
        }

        /// <summary>
        /// Gives back all the pieces table's records.
        /// </summary>
        /// <returns>the queryable pieces table.</returns>
        public IQueryable<pieces> ReadAll()
        {
            return this.dbcontext.pieces;
        }

        /// <summary>
        /// Update an existing piece record.
        /// </summary>
        /// <param name="data">The piece wanted to be updated.</param>
        /// <returns>The updated piece.</returns>
        public pieces Update(pieces data)
        {
            throw new NotImplementedException();

            // TODO: Implement if needed.
        }

        /// <summary>
        /// Returns with the pieces records which has the gived book id.
        /// </summary>
        /// <param name="id">The given book id.</param>
        /// <returns>The quaryable list of the result.</returns>
        public IQueryable<pieces> BookGetById(decimal id)
        {
            return this.dbcontext.pieces.Where(x => x.booksId == id);
        }

        /// <summary>
        /// Returns with the pieces records which has the gived store id.
        /// </summary>
        /// <param name="id">The given store id.</param>
        /// <returns>The quaryable list of the result.</returns>
        public IQueryable<pieces> StoreGetById(decimal id)
        {
            return this.dbcontext.pieces.Where(x => x.storeId == id);
        }
    }
}
