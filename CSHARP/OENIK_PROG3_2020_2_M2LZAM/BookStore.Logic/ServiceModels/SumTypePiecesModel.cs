﻿// <copyright file="SumTypePiecesModel.cs" company="DorogiTamas">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookStore.Logic.ServiceModels
{
    /// <summary>
    /// Model class of the query which returns with the list of how many books we have grouped by genre.
    /// </summary>
    public class SumTypePiecesModel
    {
        /// <summary>
        /// Gets or Sets the type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or Sets how many pieces of the type.
        /// </summary>
        public decimal Pieces { get; set; }
    }
}
