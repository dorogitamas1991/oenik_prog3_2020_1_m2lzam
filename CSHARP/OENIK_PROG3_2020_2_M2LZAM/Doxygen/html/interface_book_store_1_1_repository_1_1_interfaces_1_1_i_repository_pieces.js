var interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository_pieces =
[
    [ "BookGetById", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository_pieces.html#a983fcfb3c465bcbcf14ff51bec567a60", null ],
    [ "Create", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository_pieces.html#a5c0cb6de4eb93003c33dd2c13ec14b4e", null ],
    [ "DeleteBook", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository_pieces.html#ad9eb74b8679ff60922137445f86f7f8b", null ],
    [ "DeleteStore", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository_pieces.html#a2694ff8ff8863d4ba6498f537c3df44c", null ],
    [ "ReadAll", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository_pieces.html#aa1d3dcf37067ed518eac99fc206d8409", null ],
    [ "StoreGetById", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository_pieces.html#a25c793d42e478b61880035ff97536074", null ],
    [ "Update", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository_pieces.html#adcd0cf03fa3cdaa775ca1db17d3d0b49", null ]
];