var searchData=
[
  ['authorrepository_122',['AuthorRepository',['../class_book_store_1_1_repository_1_1_repositories_1_1_author_repository.html#aefb07b6f266be17f6fc0323b78fe1f64',1,'BookStore::Repository::Repositories::AuthorRepository']]],
  ['authorsearchbasedontitle_123',['AuthorSearchBasedOnTitle',['../interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#a7d1d9a48a5c94133d28862de40cb88f9',1,'BookStore.Logic.Interfaces.ILogic.AuthorSearchBasedOnTitle()'],['../class_book_store_1_1_logic_1_1_services_1_1_business_logic.html#aa9c9f37d999fbddc05751797d07b84a0',1,'BookStore.Logic.Services.BusinessLogic.AuthorSearchBasedOnTitle()']]],
  ['averageagestest_124',['AverageAgesTest',['../class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#a977a482eb0fb4703545f9ef16651d5c9',1,'BookStore::Logic::Tests::BusinessLogicTests']]],
  ['avgofwritersagebygenre_125',['AVGofWritersAgeByGenre',['../interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#a5a18c6f1a2f3b24898f85351b8985a5f',1,'BookStore.Logic.Interfaces.ILogic.AVGofWritersAgeByGenre()'],['../class_book_store_1_1_logic_1_1_services_1_1_business_logic.html#ac0198079b811cc056377e25676cef9ba',1,'BookStore.Logic.Services.BusinessLogic.AVGofWritersAgeByGenre()']]]
];
