﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookStore.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Generic Interface for Book,Store and Author Repositories.
    /// </summary>
    /// <typeparam name="T">T generic type.</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Read all what is in the table.
        /// </summary>
        /// <returns>Returns an IQueryable.</returns>
        IQueryable<T> ReadAll();

        /// <summary>
        /// For puttung the new T type element into the database.
        /// </summary>
        /// <param name="data">The element that should go into the database.</param>
        void Create(T data);

        /// <summary>
        /// For updating an existing T element in the database.
        /// </summary>
        /// <param name="data">The element that should go into the database.</param>
        void Update(T data);

        /// <summary>
        /// For Deleting an element from the database.
        /// </summary>
        /// <param name="id">The Id of the T tyoe element which have to be deleted.</param>
        void Delete(decimal id);

        /// <summary>
        /// For giving back an element from the database based on the given Id.
        /// </summary>
        /// <param name="id">The Id of the element.</param>
        /// <returns>Returns with the element.</returns>
        T GetById(decimal id);
    }
}
