var class_book_store_1_1_repository_1_1_books_repository =
[
    [ "BooksRepository", "class_book_store_1_1_repository_1_1_books_repository.html#a1d47dbd1ad1571045874e8d42c678bbf", null ],
    [ "Create", "class_book_store_1_1_repository_1_1_books_repository.html#a396f068993875770ebe77e4d3f5acdef", null ],
    [ "Delete", "class_book_store_1_1_repository_1_1_books_repository.html#a28200fa110b34ebc07197a10a4f2bfc0", null ],
    [ "GetById", "class_book_store_1_1_repository_1_1_books_repository.html#a087610fb8a5ad296765f88f24336a9fa", null ],
    [ "ReadAll", "class_book_store_1_1_repository_1_1_books_repository.html#afbd38525cbdbd14a40c463eaa16814bf", null ],
    [ "Update", "class_book_store_1_1_repository_1_1_books_repository.html#adb0d9f5e27643f7ed5ac0c68e3dcb3f9", null ]
];