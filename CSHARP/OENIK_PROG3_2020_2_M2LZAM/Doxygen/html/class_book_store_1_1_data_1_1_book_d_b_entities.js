var class_book_store_1_1_data_1_1_book_d_b_entities =
[
    [ "BookDBEntities", "class_book_store_1_1_data_1_1_book_d_b_entities.html#ab738bf7fa798eedd7808e2ff966c8e3f", null ],
    [ "OnModelCreating", "class_book_store_1_1_data_1_1_book_d_b_entities.html#a9a87a3227fb3ecae4c2b21edfc5847be", null ],
    [ "authors", "class_book_store_1_1_data_1_1_book_d_b_entities.html#ac67db6b281752cd5ed001782ab8109c0", null ],
    [ "books", "class_book_store_1_1_data_1_1_book_d_b_entities.html#afd8d4322f9c4ecb2430e742155d9d669", null ],
    [ "pieces", "class_book_store_1_1_data_1_1_book_d_b_entities.html#a0d65530bf61dc609db1299f87ac55b52", null ],
    [ "store", "class_book_store_1_1_data_1_1_book_d_b_entities.html#a9716160712a8fdc4ce9a75e37cc88162", null ]
];