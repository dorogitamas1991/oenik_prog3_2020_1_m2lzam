var searchData=
[
  ['bookgetbyid_126',['BookGetById',['../interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository_pieces.html#a983fcfb3c465bcbcf14ff51bec567a60',1,'BookStore.Repository.Interfaces.IRepositoryPieces.BookGetById()'],['../class_book_store_1_1_repository_1_1_repositories_1_1_pieces_repository.html#a6629180b8deeca9e3ac83190dbcc9fbc',1,'BookStore.Repository.Repositories.PiecesRepository.BookGetById()']]],
  ['bookpiecesumbytitletest_127',['BookPieceSumByTitleTest',['../class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html#a2d79e4b94e59a146ffda73aa78747d93',1,'BookStore::Logic::Tests::BusinessLogicTests']]],
  ['booksearchbasedonname_128',['BookSearchBasedOnName',['../interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#abad163373600f0e89e8164fde7ac6c53',1,'BookStore.Logic.Interfaces.ILogic.BookSearchBasedOnName()'],['../class_book_store_1_1_logic_1_1_services_1_1_business_logic.html#af00571d6f0efe2ced0e460741b10aec5',1,'BookStore.Logic.Services.BusinessLogic.BookSearchBasedOnName()']]],
  ['booksrepository_129',['BooksRepository',['../class_book_store_1_1_repository_1_1_books_repository.html#a1d47dbd1ad1571045874e8d42c678bbf',1,'BookStore::Repository::BooksRepository']]],
  ['businesslogic_130',['BusinessLogic',['../class_book_store_1_1_logic_1_1_services_1_1_business_logic.html#ab619cc3ab28370494b9d34064ffdab6f',1,'BookStore::Logic::Services::BusinessLogic']]]
];
