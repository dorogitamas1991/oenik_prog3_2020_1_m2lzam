var namespace_book_store_1_1_data =
[
    [ "authors", "class_book_store_1_1_data_1_1authors.html", "class_book_store_1_1_data_1_1authors" ],
    [ "BookDBEntities", "class_book_store_1_1_data_1_1_book_d_b_entities.html", "class_book_store_1_1_data_1_1_book_d_b_entities" ],
    [ "books", "class_book_store_1_1_data_1_1books.html", "class_book_store_1_1_data_1_1books" ],
    [ "pieces", "class_book_store_1_1_data_1_1pieces.html", "class_book_store_1_1_data_1_1pieces" ],
    [ "store", "class_book_store_1_1_data_1_1store.html", "class_book_store_1_1_data_1_1store" ]
];