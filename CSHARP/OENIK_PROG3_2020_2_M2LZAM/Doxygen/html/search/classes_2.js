var searchData=
[
  ['ilogic_98',['ILogic',['../interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html',1,'BookStore::Logic::Interfaces']]],
  ['irepository_99',['IRepository',['../interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'BookStore::Repository::Interfaces']]],
  ['irepository_3c_20authors_20_3e_100',['IRepository&lt; authors &gt;',['../interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'BookStore::Repository::Interfaces']]],
  ['irepository_3c_20books_20_3e_101',['IRepository&lt; books &gt;',['../interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'BookStore::Repository::Interfaces']]],
  ['irepository_3c_20bookstore_3a_3adata_3a_3aauthors_20_3e_102',['IRepository&lt; BookStore::Data::authors &gt;',['../interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'BookStore::Repository::Interfaces']]],
  ['irepository_3c_20bookstore_3a_3adata_3a_3abooks_20_3e_103',['IRepository&lt; BookStore::Data::books &gt;',['../interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'BookStore::Repository::Interfaces']]],
  ['irepository_3c_20bookstore_3a_3adata_3a_3astore_20_3e_104',['IRepository&lt; BookStore::Data::store &gt;',['../interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'BookStore::Repository::Interfaces']]],
  ['irepository_3c_20store_20_3e_105',['IRepository&lt; store &gt;',['../interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html',1,'BookStore::Repository::Interfaces']]],
  ['irepositorypieces_106',['IRepositoryPieces',['../interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository_pieces.html',1,'BookStore::Repository::Interfaces']]]
];
