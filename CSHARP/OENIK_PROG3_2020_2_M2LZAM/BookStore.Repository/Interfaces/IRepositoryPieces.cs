﻿// <copyright file="IRepositoryPieces.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookStore.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BookStore.Data;

    /// <summary>
    /// Interface for the pieces repository.
    /// </summary>
    public interface IRepositoryPieces
    {
        /// <summary>
        /// Get all the elements from the pieces table.
        /// </summary>
        /// <returns>Returns the Queryable pieces table.</returns>
        IQueryable<pieces> ReadAll();

        /// <summary>
        /// Gives the newly created pieces element to the datacontext.
        /// </summary>
        /// <param name="data">The pieces element that have to be created.</param>
        void Create(pieces data);

        /// <summary>
        /// Updates an existing pieces element.
        /// </summary>
        /// <param name="data">The new version of the element.</param>
        /// <returns>Returns the new version of the element.</returns>
        pieces Update(pieces data);

        /// <summary>
        /// Deletes a records based on its  book id.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        void DeleteBook(decimal id);

        /// <summary>
        /// Deletes a records based on its  store id.
        /// </summary>
        /// <param name="id">The id of the store.</param>
        void DeleteStore(decimal id);

        /// <summary>
        /// For get all the elements from the pieces table which has a certain store id.
        /// </summary>
        /// <param name="id">The id of the store we need.</param>
        /// <returns>Return a queryable list of pieces records.</returns>
        IQueryable<pieces> StoreGetById(decimal id);

        /// <summary>
        /// For get all the elements from the pieces table which has a certain book id.
        /// </summary>
        /// <param name="id">The id of the book we need.</param>
        /// <returns>Return a queryable list of pieces records.</returns>
        IQueryable<pieces> BookGetById(decimal id);
    }
}
