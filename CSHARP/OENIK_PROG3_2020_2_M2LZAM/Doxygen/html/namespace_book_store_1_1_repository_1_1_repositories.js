var namespace_book_store_1_1_repository_1_1_repositories =
[
    [ "AuthorRepository", "class_book_store_1_1_repository_1_1_repositories_1_1_author_repository.html", "class_book_store_1_1_repository_1_1_repositories_1_1_author_repository" ],
    [ "PiecesRepository", "class_book_store_1_1_repository_1_1_repositories_1_1_pieces_repository.html", "class_book_store_1_1_repository_1_1_repositories_1_1_pieces_repository" ],
    [ "StoreRepository", "class_book_store_1_1_repository_1_1_repositories_1_1_store_repository.html", "class_book_store_1_1_repository_1_1_repositories_1_1_store_repository" ]
];