var interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic =
[
    [ "AuthorSearchBasedOnTitle", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#a7d1d9a48a5c94133d28862de40cb88f9", null ],
    [ "AVGofWritersAgeByGenre", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#a5a18c6f1a2f3b24898f85351b8985a5f", null ],
    [ "BookSearchBasedOnName", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#abad163373600f0e89e8164fde7ac6c53", null ],
    [ "CreateAuthor", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#ab99c3ec3489e25c00493259fb4dc552a", null ],
    [ "CreateBook", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#ace8ad80a8279817d6b812ed3cfbe0164", null ],
    [ "CreatePiece", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#a97c50cc4aca0e6cdd7e135c0d6c77c3f", null ],
    [ "CreateStore", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#a59ef887aa7f15b66283af67ef7f0a2de", null ],
    [ "DeleteAuthor", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#a7bc275de5d3b03199a8eefd0c6a0e8fe", null ],
    [ "DeleteBook", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#acdda3e7f38fa26c0624d87353bc533de", null ],
    [ "DeleteBookPieces", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#ad813319b1a16f0dbd74cbcad1aa6151b", null ],
    [ "DeleteStore", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#afd1478008dd5ba2c97b0fc08176949df", null ],
    [ "DeleteStorePieces", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#a8d41d947015fae524329e8fdd597a685", null ],
    [ "GetAuthors", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#ae07a4653bad5758f751cf567c763c4f1", null ],
    [ "GetAuthorsById", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#aecb06979d817ac8ff618f1a1d2033de6", null ],
    [ "GetBooks", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#ad420b845707ffab8fc616200a23f24a6", null ],
    [ "GetBooksById", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#a631ff1ee08d89cf60e97d8c4d5ba3784", null ],
    [ "GetStoreById", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#a1cbc8db9a164dbcc7d429d3528418ebf", null ],
    [ "GetStores", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#aebc5b42c74d5519623925c266249d924", null ],
    [ "PiecesInWhichStore", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#aca4ba72fab6c06bc782c40c74167de64", null ],
    [ "SumOfBooksByTitle", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#a9a6af5110636e3364c53969c317829d0", null ],
    [ "SumOfTypePieces", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#afcb4777ed5b61442b06965b76ec4aaa4", null ],
    [ "UpdateAuthor", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#a1ad0249e2a9636db60c85ef3c771878b", null ],
    [ "UpdateBook", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#a99c4b8fa54fffe19cf06c23b5df99677", null ],
    [ "UpdatePiece", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#a38fb453c1455e328fe14d89d930e14c3", null ],
    [ "UpdateStore", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html#a559793bb2e65a52a24bfcfe2401ca8d5", null ]
];