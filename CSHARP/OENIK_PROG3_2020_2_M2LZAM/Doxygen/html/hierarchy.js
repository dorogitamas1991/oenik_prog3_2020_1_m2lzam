var hierarchy =
[
    [ "BookStore.Data.authors", "class_book_store_1_1_data_1_1authors.html", null ],
    [ "BookStore.Logic.ServiceModels.AverageOfWritersAgeInGenreModel", "class_book_store_1_1_logic_1_1_service_models_1_1_average_of_writers_age_in_genre_model.html", null ],
    [ "BookStore.Logic.ServiceModels.BookPiecesSumByTitle", "class_book_store_1_1_logic_1_1_service_models_1_1_book_pieces_sum_by_title.html", null ],
    [ "BookStore.Data.books", "class_book_store_1_1_data_1_1books.html", null ],
    [ "BookStore.Logic.Tests.BusinessLogicTests", "class_book_store_1_1_logic_1_1_tests_1_1_business_logic_tests.html", null ],
    [ "DbContext", null, [
      [ "BookStore.Data.BookDBEntities", "class_book_store_1_1_data_1_1_book_d_b_entities.html", null ]
    ] ],
    [ "BookStore.Logic.Interfaces.ILogic", "interface_book_store_1_1_logic_1_1_interfaces_1_1_i_logic.html", [
      [ "BookStore.Logic.Services.BusinessLogic", "class_book_store_1_1_logic_1_1_services_1_1_business_logic.html", null ]
    ] ],
    [ "BookStore.Repository.Interfaces.IRepository< T >", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html", null ],
    [ "BookStore.Repository.Interfaces.IRepository< authors >", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "BookStore.Repository.Repositories.AuthorRepository", "class_book_store_1_1_repository_1_1_repositories_1_1_author_repository.html", null ]
    ] ],
    [ "BookStore.Repository.Interfaces.IRepository< books >", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "BookStore.Repository.BooksRepository", "class_book_store_1_1_repository_1_1_books_repository.html", null ]
    ] ],
    [ "BookStore.Repository.Interfaces.IRepository< BookStore.Data.authors >", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html", null ],
    [ "BookStore.Repository.Interfaces.IRepository< BookStore.Data.books >", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html", null ],
    [ "BookStore.Repository.Interfaces.IRepository< BookStore.Data.store >", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html", null ],
    [ "BookStore.Repository.Interfaces.IRepository< store >", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "BookStore.Repository.Repositories.StoreRepository", "class_book_store_1_1_repository_1_1_repositories_1_1_store_repository.html", null ]
    ] ],
    [ "BookStore.Repository.Interfaces.IRepositoryPieces", "interface_book_store_1_1_repository_1_1_interfaces_1_1_i_repository_pieces.html", [
      [ "BookStore.Repository.Repositories.PiecesRepository", "class_book_store_1_1_repository_1_1_repositories_1_1_pieces_repository.html", null ]
    ] ],
    [ "BookStore.Data.pieces", "class_book_store_1_1_data_1_1pieces.html", null ],
    [ "BookStore.Logic.ServiceModels.PiecesInWhichStoreModel", "class_book_store_1_1_logic_1_1_service_models_1_1_pieces_in_which_store_model.html", null ],
    [ "BookStore.Data.store", "class_book_store_1_1_data_1_1store.html", null ],
    [ "BookStore.Logic.ServiceModels.SumTypePiecesModel", "class_book_store_1_1_logic_1_1_service_models_1_1_sum_type_pieces_model.html", null ]
];