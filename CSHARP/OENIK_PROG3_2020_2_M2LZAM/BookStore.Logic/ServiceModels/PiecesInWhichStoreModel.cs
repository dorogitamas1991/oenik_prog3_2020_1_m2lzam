﻿// <copyright file="PiecesInWhichStoreModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookStore.Logic.ServiceModels
{
    /// <summary>
    /// A model for a query result which gives back how many pieces exists in a certain store.
    /// </summary>
    public class PiecesInWhichStoreModel
    {
        /// <summary>
        /// Gets or sets the store's name.
        /// </summary>
        public string StoreName { get; set; }

        /// <summary>
        /// Gets or sets the store's address.
        /// </summary>
        public string StoreAddress { get; set; }

        /// <summary>
        /// Gets or sets how many pieces the store have.
        /// </summary>
        public decimal Pieces { get; set; }
    }
}
