﻿// <copyright file="AverageOfWritersAgeInGenreModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookStore.Logic.ServiceModels
{
    /// <summary>
    /// Average age by genre query model.
    /// </summary>
    public class AverageOfWritersAgeInGenreModel
    {
        /// <summary>
        /// Gets or Sets the average age.
        /// </summary>
        public decimal Age { get; set; }

        /// <summary>
        /// Gets or Sets the genre.
        /// </summary>
        public string Genre { get; set; }
    }
}
