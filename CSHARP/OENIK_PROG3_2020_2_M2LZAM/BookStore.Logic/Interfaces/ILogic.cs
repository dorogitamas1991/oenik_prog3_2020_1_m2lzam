﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookStore.Logic.Interfaces
{
    using System.Collections.Generic;
    using BookStore.Data;
    using BookStore.Logic.ServiceModels;

    /// <summary>
    /// Interface for defining the methods of the BusinessLogic.
    /// </summary>
    public interface ILogic
    {
        // CRUD methods
        // Book methods

        /// <summary>
        /// For getting the books.
        /// </summary>
        /// <returns>All the book list.</returns>
        List<books> GetBooks();

        /// <summary>
        /// For getting a book based on id.
        /// </summary>
        /// <param name="id">The book's id.</param>
        /// <returns>The wanted book.</returns>
        books GetBooksById(decimal id);

        /// <summary>
        /// For Creating a book.
        /// </summary>
        /// <param name="book">The Creatable book.</param>
        void CreateBook(books book);

        /// <summary>
        /// For updating a book.
        /// </summary>
        /// <param name="book">The new book which will change the updatable.</param>
        void UpdateBook(books book);

        /// <summary>
        /// For deleting a book based on its id.
        /// </summary>
        /// <param name="id">The deletable book's id.</param>
        void DeleteBook(decimal id);

        // Írókra vonatkozó metódusok

        /// <summary>
        /// For getting all authors.
        /// </summary>
        /// <returns>The list of all authors.</returns>
        List<authors> GetAuthors();

        /// <summary>
        /// For getting an author by its id.
        /// </summary>
        /// <param name="id">The wanted author's id.</param>
        /// <returns>The wanted author.</returns>
        authors GetAuthorsById(decimal id);

        /// <summary>
        /// For creating an author.
        /// </summary>
        /// <param name="author">The author which we want in the database.</param>
        void CreateAuthor(authors author);

        /// <summary>
        /// For updating an existing author.
        /// </summary>
        /// <param name="author">The author which will be the updated one.</param>
        void UpdateAuthor(authors author);

        /// <summary>
        /// For deleting an author based on id.
        /// </summary>
        /// <param name="id">The id of the author.</param>
        void DeleteAuthor(decimal id);

        // Boltokra vonatkozó metódusok

        /// <summary>
        /// For getting all the stores in the store table.
        /// </summary>
        /// <returns>A list of the stores.</returns>
        List<store> GetStores();

        /// <summary>
        /// For getting a store by its id.
        /// </summary>
        /// <param name="id">The store's id.</param>
        /// <returns>The wanted store.</returns>
        store GetStoreById(decimal id);

        /// <summary>
        /// For creating a store.
        /// </summary>
        /// <param name="store">The store we want to put in the database.</param>
        void CreateStore(store store);

        /// <summary>
        /// For updating a store.
        /// </summary>
        /// <param name="store">The store we wannt to be the updated.</param>
        void UpdateStore(store store);

        /// <summary>
        /// For deleting a store by its id.
        /// </summary>
        /// <param name="id">The id of the store wanted to be deleted.</param>
        void DeleteStore(decimal id);

        // Darabszámokra vonatkozó metódusok

        /// <summary>
        /// For creating a new piece record.
        /// </summary>
        /// <param name="piece">The piece we want to add.</param>
        void CreatePiece(pieces piece);

        /// <summary>
        /// For updating a piece record.
        /// </summary>
        /// <param name="piece">The piece we want to be the updated.</param>
        /// <returns>The updated piece.</returns>
        pieces UpdatePiece(pieces piece);

        /// <summary>
        /// For deleting pieces by book id.
        /// </summary>
        /// <param name="id">The book id.</param>
        void DeleteBookPieces(decimal id);

        /// <summary>
        /// For deleting pieces by store id.
        /// </summary>
        /// <param name="id">The store id.</param>
        void DeleteStorePieces(decimal id);

        // Egyéb lekérdezések (nem CRUD)

        /// <summary>
        /// Gives back the books written by the given author name.
        /// </summary>
        /// <param name="authorname">Author's name.</param>
        /// <returns>The list of the books.</returns>
        List<string> BookSearchBasedOnName(string authorname);

        /// <summary>
        /// Gives back the author of the given book.
        /// </summary>
        /// <param name="title">The book's title.</param>
        /// <returns>The author's name.</returns>
        string AuthorSearchBasedOnTitle(string title);

        /// <summary>
        /// Gives a List which contains which store and how many pieces they have from a certain book by it's given book title.
        /// </summary>
        /// <param name="title">The book's title.</param>
        /// <returns>A list of PiecesInWhichStoreModel.</returns>
        List<PiecesInWhichStoreModel> PiecesInWhichStore(string title);

        /// <summary>
        /// Query which will return with a list of how many pieces we have from a certain genre of books.
        /// </summary>
        /// <returns>The list of the query result model.</returns>
        List<SumTypePiecesModel> SumOfTypePieces();

        /// <summary>
        /// Query which will return with a list of how many pieces we have from a certain title of books.
        /// </summary>
        /// <returns>The list of the query result model.</returns>
        List<BookPiecesSumByTitle> SumOfBooksByTitle();

        /// <summary>
        /// Query which will return with a list of the average age of authors grouped by their ganres.
        /// </summary>
        /// <returns>The list of the query result model.</returns>
        List<AverageOfWritersAgeInGenreModel> AVGofWritersAgeByGenre();
    }
}
