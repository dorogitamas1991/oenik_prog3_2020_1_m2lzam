var class_book_store_1_1_data_1_1authors =
[
    [ "authors", "class_book_store_1_1_data_1_1authors.html#a39a5ee0e61ac07c89edba5309e2d96d7", null ],
    [ "age", "class_book_store_1_1_data_1_1authors.html#aefe365236bab10f77782713589c58586", null ],
    [ "alive", "class_book_store_1_1_data_1_1authors.html#a9c7bdb9aae984c1eb8769994e3935c4d", null ],
    [ "authorId", "class_book_store_1_1_data_1_1authors.html#a7a5f42b984694304676e4ea51e33b5de", null ],
    [ "authorName", "class_book_store_1_1_data_1_1authors.html#ac64d1544a293b265d8572bd36755b99a", null ],
    [ "books", "class_book_store_1_1_data_1_1authors.html#ac6029b75da6c03ce31d5048fa39e00a5", null ],
    [ "gender", "class_book_store_1_1_data_1_1authors.html#aedae616684e03c6cd6d297fb28a27d37", null ],
    [ "nationality", "class_book_store_1_1_data_1_1authors.html#a88a9283aeb0fc22e0bb2ec280b0364b2", null ]
];