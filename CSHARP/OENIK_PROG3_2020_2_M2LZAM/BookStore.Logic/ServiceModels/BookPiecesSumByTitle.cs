﻿// <copyright file="BookPiecesSumByTitle.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookStore.Logic.ServiceModels
{
    /// <summary>
    /// Model of the query which returns with how many pieces we have from titles.
    /// </summary>
    public class BookPiecesSumByTitle
    {
        /// <summary>
        /// Gets or Sets the title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or Sets the pieces.
        /// </summary>
        public decimal Pieces { get; set; }
    }
}
