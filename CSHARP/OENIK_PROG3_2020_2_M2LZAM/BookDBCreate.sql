﻿IF OBJECT_ID('pieces', 'U') IS NOT NULL DROP TABLE pieces;
IF OBJECT_ID('books', 'U') IS NOT NULL DROP TABLE books;
IF OBJECT_ID('authors', 'U') IS NOT NULL DROP TABLE authors;
IF OBJECT_ID('store', 'U') IS NOT NULL DROP TABLE store; 

CREATE TABLE authors (
    authorId NUMERIC(3) IDENTITY(1,1) NOT NULL, 
    authorName VARCHAR(200),
    nationality VARCHAR(200),
    age NUMERIC (4),
    gender BIT DEFAULT (0),
    alive BIT DEFAULT (0)
    CONSTRAINT AUTHORS_PRIMARY_KEY PRIMARY KEY (authorId) );


 
CREATE TABLE books (
    bookId NUMERIC(3)  IDENTITY(1,1) NOT NULL,
    bookTitle VARCHAR(200),
    booktype VARCHAR(200),
    writerId NUMERIC(3),
    pages NUMERIC (4),
    isbn VARCHAR(200),
    CONSTRAINT FK_AUTHORS_ID FOREIGN KEY (writerId)
    REFERENCES authors (authorId),
    CONSTRAINT BOOKS_PRIMARY_KEY PRIMARY KEY (bookId));


 
CREATE TABLE store (
    storeId NUMERIC(3) IDENTITY(1,1) NOT NULL,
    storeAddress VARCHAR(200),
    storename VARCHAR(200),
     megjegyzés VARCHAR(200),
    rating NUMERIC (3),
    CONSTRAINT STORE_PRIMARY_KEY PRIMARY KEY (storeId) );
    

CREATE TABLE pieces (
    booksId NUMERIC(3) NOT NULL,
    storeId NUMERIC(3) NOT NULL,
    pieces NUMERIC(4),
    CONSTRAINT FK_BOOK_ID FOREIGN KEY (booksId)
    REFERENCES books (bookId),
    CONSTRAINT FK_STORE_ID FOREIGN KEY (storeId)
    REFERENCES store (storeId));
   
INSERT INTO authors VALUES ('J.R.R Tolkien','Angol',81,0,0);
INSERT INTO authors VALUES ('George R.R Martin','Amerikai',71,0,1);
INSERT INTO authors VALUES ('Suzanne Collins','Amerikai',57,1,1);
INSERT INTO authors VALUES ('Jo Nesbo','Norvég',60,0,1);
INSERT INTO authors VALUES ('J.K. Rowling','Angol',54,1,1);
INSERT INTO authors VALUES ('Isaac Asimov','Orosz',72,0,0);

INSERT INTO books VALUES ('A gyűrűk Ura','Fantasy',1,1800,'0618343997');
INSERT INTO books VALUES ('Trónok harca','Fantasy',2,925,'9789634470878');
INSERT INTO books VALUES ('Az éhezők viadala','Young-Adult',3,390,'9786155049675');
INSERT INTO books VALUES ('Leopárd','Skandináv krimi',4,560,'9789633240540');
INSERT INTO books VALUES ('Harry Potter és a Bölcsek köve','Fantasy',5,270,'9789633246924');
INSERT INTO books VALUES ('Alapítvány','Sci-fi',6,255,'9789636894221');
INSERT INTO books VALUES ('Hóember','Skandináv krimi',4,416,'9789633245231');
INSERT INTO books VALUES ('Robottörténetek','Sci-fi',6,400,'9630949385 ');

INSERT INTO store VALUES ('Mátyás Király u 43.','Libri','Tavaszi akcio',4);
INSERT INTO store VALUES ('Szent Korona u.','Libri','Eladót felveszünk',5);
INSERT INTO store VALUES ('Radnóti Miklód u. 4.','Alexandra',' Végkiárusítás',3);

INSERT INTO pieces VALUES (1,1,120);
INSERT INTO pieces VALUES (1,2,12);
INSERT INTO pieces VALUES (1,3,1240);
INSERT INTO pieces VALUES (2,1,1220);
INSERT INTO pieces VALUES (2,2,5120);
INSERT INTO pieces VALUES (2,3,1202);
INSERT INTO pieces VALUES (3,1,1220);
INSERT INTO pieces VALUES (3,2,1240);
INSERT INTO pieces VALUES (3,3,1250);
INSERT INTO pieces VALUES (4,1,6120);
INSERT INTO pieces VALUES (4,2,1120);
INSERT INTO pieces VALUES (4,3,1120);
INSERT INTO pieces VALUES (5,1,1520);
INSERT INTO pieces VALUES (5,2,1206);
INSERT INTO pieces VALUES (5,3,1230);
INSERT INTO pieces VALUES (6,1,1210);
INSERT INTO pieces VALUES (6,2,1220);
INSERT INTO pieces VALUES (6,3,1420);
INSERT INTO pieces VALUES (7,1,10);
INSERT INTO pieces VALUES (7,2,430);
INSERT INTO pieces VALUES (7,3,50);
INSERT INTO pieces VALUES (8,1,10);
INSERT INTO pieces VALUES (8,2,413);
INSERT INTO pieces VALUES (8,3,140);

