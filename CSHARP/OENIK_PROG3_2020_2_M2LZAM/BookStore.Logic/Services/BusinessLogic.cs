﻿// <copyright file="BusinessLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookStore.Logic.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BookStore.Data;
    using BookStore.Logic.Interfaces;
    using BookStore.Logic.ServiceModels;
    using BookStore.Repository;
    using BookStore.Repository.Interfaces;
    using BookStore.Repository.Repositories;

    /// <summary>
    /// The BusinessLogic that implements ILogic interface.
    /// </summary>
    public class BusinessLogic : ILogic
    {
        private IRepository<books> bookRepository;
        private IRepository<authors> authorRepository;
        private IRepository<store> storeRepository;
        private IRepositoryPieces piecesRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessLogic"/> class.
        /// </summary>
        /// <param name="bookrepo">Dependency Injection bookrepository.</param>
        /// <param name="authorrepo">Dependency Injection authorrepository.</param>
        /// <param name="storerepo">Dependency Injection store repository.</param>
        /// <param name="piecerepo">Dependency Injection pieces repository.</param>
        public BusinessLogic(IRepository<books> bookrepo, IRepository<authors> authorrepo, IRepository<store> storerepo, IRepositoryPieces piecerepo)
        {
            this.bookRepository = bookrepo;
            this.authorRepository = authorrepo;
            this.storeRepository = storerepo;
            this.piecesRepository = piecerepo;
        }

        /// <summary>
        /// Factory method for creating the Business logic without static reference to the repositories.
        /// </summary>
        /// <returns>New Business Logic class with the repositories.</returns>
        public static ILogic CreateBL()
        {
            BookDBEntities dbcontext = new BookDBEntities();
            return new BusinessLogic(new BooksRepository(dbcontext), new AuthorRepository(dbcontext), new StoreRepository(dbcontext), new PiecesRepository(dbcontext));
        }

        /// <summary>
        /// Get the list of all books.
        /// </summary>
        /// <returns>The list of all books.</returns>
        public List<books> GetBooks()
        {
            return this.bookRepository.ReadAll().ToList();
        }

        /// <summary>
        /// Get the list of all authors.
        /// </summary>
        /// <returns>The list of all authors.</returns>
        public List<authors> GetAuthors()
        {
            return this.authorRepository.ReadAll().ToList();
        }

        /// <summary>
        /// Get the list of all stores.
        /// </summary>
        /// <returns>The list of all stores.</returns>
        public List<store> GetStores()
        {
            return this.storeRepository.ReadAll().ToList();
        }

        /// <summary>
        /// Get a book by its id.
        /// </summary>
        /// <param name="id">The wanted book's id.</param>
        /// <returns>The wanted book.</returns>
        public books GetBooksById(decimal id)
        {
            return this.bookRepository.GetById(id);
        }

        /// <summary>
        /// Get an author by its id.
        /// </summary>
        /// <param name="id">The author's id.</param>
        /// <returns>The wanted author.</returns>
        public authors GetAuthorsById(decimal id)
        {
            return this.authorRepository.GetById(id);
        }

        /// <summary>
        /// Get a store by its id.
        /// </summary>
        /// <param name="id">The wanted store's id.</param>
        /// <returns>The wanted store.</returns>
        public store GetStoreById(decimal id)
        {
            return this.storeRepository.GetById(id);
        }

        /// <summary>
        /// Invites the book repository's create method.
        /// </summary>
        /// <param name="book">The book for the repository.</param>
        public void CreateBook(books book)
        {
            this.bookRepository.Create(book);
        }

        /// <summary>
        /// Invites the author repository's create method.
        /// </summary>
        /// <param name="author">The author for the repository.</param>
        public void CreateAuthor(authors author)
        {
            this.authorRepository.Create(author);
        }

        /// <summary>
        /// Invites the store repository's create method.
        /// </summary>
        /// <param name="store">The store for the repository.</param>
        public void CreateStore(store store)
        {
            this.storeRepository.Create(store);
        }

        /// <summary>
        /// Invites the book repository's Update method.
        /// </summary>
        /// <param name="book">The book for the repository.</param>
        public void UpdateBook(books book)
        {
            var bookfromdb = this.bookRepository.GetById(book.bookId);
            bookfromdb = book;
            this.bookRepository.Update(book);
        }

        /// <summary>
        /// Invites the author repository's Update method.
        /// </summary>
        /// <param name="author">The author for the repository.</param>
        public void UpdateAuthor(authors author)
        {
            var authorfromdb = this.authorRepository.GetById(author.authorId);
            authorfromdb = author;
            this.authorRepository.Update(author);
        }

        /// <summary>
        /// Invites the store repository's Update method.
        /// </summary>
        /// <param name="store">The store for the repository.</param>
        public void UpdateStore(store store)
        {
            var storefromdb = this.storeRepository.GetById(store.storeId);
            storefromdb = store;
            this.storeRepository.Update(store);
        }

        /// <summary>
        /// Invites the book repository's delete method.
        /// </summary>
        /// <param name="id">The id of the book wanted to be deleted.</param>
        public void DeleteBook(decimal id)
        {
            this.bookRepository.Delete(id);
        }

        /// <summary>
        /// Invites the author repository's delete method.
        /// </summary>
        /// <param name="id">The id of the author wanted to be deleted.</param>
        public void DeleteAuthor(decimal id)
        {
            this.authorRepository.Delete(id);
        }

        /// <summary>
        /// Invites the store repository's delete method.
        /// </summary>
        /// <param name="id">The id of the store wanted to be deleted.</param>
        public void DeleteStore(decimal id)
        {
            this.storeRepository.Delete(id);
        }

        /// <summary>
        /// Invites the piece repository's create method.
        /// </summary>
        /// <param name="piece">The piece wanted to be created.</param>
        public void CreatePiece(pieces piece)
        {
            this.piecesRepository.Create(piece);
        }

        /// <summary>
        /// Invites the piece repository's update method. (Not implemented, just here if I need it later.)
        /// </summary>
        /// <param name="piece">The piece wanted to be updated.</param>
        /// <returns>The updated piece.</returns>
        public pieces UpdatePiece(pieces piece)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Invites the piece repository's DeleteBook method.
        /// </summary>
        /// <param name="id">The Book's id we want do delete.</param>
        public void DeleteBookPieces(decimal id)
        {
            this.piecesRepository.DeleteBook(id);
        }

        /// <summary>
        /// Invites the piece repository's DeleteStore method.
        /// </summary>
        /// <param name="id">The store's id we want do delete.</param>
        public void DeleteStorePieces(decimal id)
        {
            this.piecesRepository.DeleteStore(id);
        }

        // NON-CRUD METHODS

        /// <summary>
        /// Returns a list of the book titles writted by the given author's name.
        /// </summary>
        /// <param name="authorname">The author's name.</param>
        /// <returns>The list of the books.</returns>
        public List<string> BookSearchBasedOnName(string authorname)
        {
            var query = from books in this.bookRepository.ReadAll()
                        join authors in this.authorRepository.ReadAll() on books.writerId equals authors.authorId
                        where authors.authorName.ToLower() == authorname.ToLower()
                        select new
                        {
                            Title = books.bookTitle,
                        };
            List<string> list = new List<string>();
            foreach (var item in query)
            {
                list.Add(item.Title);
            }

            return list;
        }

        /// <summary>
        /// Tells who wrote the given book.
        /// </summary>
        /// <param name="title">The book's title.</param>
        /// <returns>The author's name.</returns>
        public string AuthorSearchBasedOnTitle(string title)
        {
            var query = from books in this.bookRepository.ReadAll()
                        join authors in this.authorRepository.ReadAll() on books.writerId equals authors.authorId
                        where books.bookTitle.ToLower() == title.ToLower()
                        select new
                        {
                           Name = authors.authorName,
                        };

            return query.First().Name;
        }

        /// <summary>
        /// Tells how many pieces a stores have from a certain book.
        /// </summary>
        /// <param name="title">The title of the book.</param>
        /// <returns>The list of store name, store address and pieces based on the book's title.</returns>
        public List<PiecesInWhichStoreModel> PiecesInWhichStore(string title)
        {
            var query = from books in this.bookRepository.ReadAll()
                        join pieces in this.piecesRepository.ReadAll() on books.bookId equals pieces.booksId
                        join store in this.storeRepository.ReadAll() on pieces.storeId equals store.storeId
                        where books.bookTitle.ToLower() == title.ToLower()
                        select new PiecesInWhichStoreModel
                        {
                            StoreName = store.storename,
                            StoreAddress = store.storeAddress,
                            Pieces = (decimal)pieces.pieces1,
                        };

            List<PiecesInWhichStoreModel> lista = new List<PiecesInWhichStoreModel>();
            foreach (var item in query)
            {
                lista.Add(item);
            }

            return lista;
        }

        /// <summary>
        /// Returs with the number of books avalaible in all stores grouped by their genre.
        /// </summary>
        /// <returns>List of the result.</returns>
        public List<SumTypePiecesModel> SumOfTypePieces()
        {
            var query = from books in this.bookRepository.ReadAll()
                        join pieces in this.piecesRepository.ReadAll() on books.bookId equals pieces.booksId
                        select new
                        {
                            Genre = books.booktype,
                            Pieces = pieces.pieces1,
                        };

            var result = from g in query
                         group g by g.Genre into groupedTypes
                         select new SumTypePiecesModel
                         {
                             Type = groupedTypes.Key,
                             Pieces = groupedTypes.Sum(x => x.Pieces).Value,
                         };

            return result.ToList();
        }

        /// <summary>
        /// Returs with the number of books avalaible in all stores grouped by their title.
        /// </summary>
        /// <returns>List of the result.</returns>
        public List<BookPiecesSumByTitle> SumOfBooksByTitle()
        {
            var query = from books in this.bookRepository.ReadAll()
                        join pieces in this.piecesRepository.ReadAll() on books.bookId equals pieces.booksId
                        select new
                        {
                            Title = books.bookTitle,
                            Pieces = pieces.pieces1,
                        };

            var result = from g in query
                         group g by g.Title into groupedTitles
                         select new BookPiecesSumByTitle
                         {
                             Title = groupedTitles.Key,
                             Pieces = groupedTitles.Sum(x => x.Pieces).Value,
                         };

            return result.ToList();
        }

        /// <summary>
        /// Average of authors ages grouped by genre.
        /// </summary>
        /// <returns>The query's result list.</returns>
        public List<AverageOfWritersAgeInGenreModel> AVGofWritersAgeByGenre()
        {
            var query = (from books in this.bookRepository.ReadAll()
                         join authors in this.authorRepository.ReadAll() on books.writerId equals authors.authorId
                         select new
                         {
                             Genre = books.booktype,
                             AuthorAge = authors.age,
                         }).Distinct();

            var result = from g in query
                         group g by g.Genre into groupedTypes
                         select new AverageOfWritersAgeInGenreModel
                         {
                             Genre = groupedTypes.Key,
                             Age = groupedTypes.Average(x => x.AuthorAge).Value,
                         };

            return result.ToList();
        }
    }
}
