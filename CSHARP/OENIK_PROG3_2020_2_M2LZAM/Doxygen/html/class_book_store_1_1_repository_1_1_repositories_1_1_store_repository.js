var class_book_store_1_1_repository_1_1_repositories_1_1_store_repository =
[
    [ "StoreRepository", "class_book_store_1_1_repository_1_1_repositories_1_1_store_repository.html#acf42ac3fe3700b9a700984822f1b2c7c", null ],
    [ "Create", "class_book_store_1_1_repository_1_1_repositories_1_1_store_repository.html#adc984bb21081ea92bc55316d04f9248e", null ],
    [ "Delete", "class_book_store_1_1_repository_1_1_repositories_1_1_store_repository.html#aae8fc44261f6c2b8dc29fa710e6edc3b", null ],
    [ "GetById", "class_book_store_1_1_repository_1_1_repositories_1_1_store_repository.html#a62561366eae3af74b0599ce37a445362", null ],
    [ "ReadAll", "class_book_store_1_1_repository_1_1_repositories_1_1_store_repository.html#a67a0550e1b95a5ec707f4c5c80c88854", null ],
    [ "Update", "class_book_store_1_1_repository_1_1_repositories_1_1_store_repository.html#ab0cc2e3b8ee88f446be6d3ccfb3dc7d4", null ]
];