var class_book_store_1_1_data_1_1books =
[
    [ "books", "class_book_store_1_1_data_1_1books.html#ac912b1d5160145aa276416c4c30fcf5b", null ],
    [ "authors", "class_book_store_1_1_data_1_1books.html#ad26f8fc56b89a97181a74a5078cb7b33", null ],
    [ "bookId", "class_book_store_1_1_data_1_1books.html#a330a7689373d221e4425dafaff3b7444", null ],
    [ "bookTitle", "class_book_store_1_1_data_1_1books.html#a476bd77691815240dbc730b30c0761af", null ],
    [ "booktype", "class_book_store_1_1_data_1_1books.html#ae63cd730938f45964b518e3c45c6a326", null ],
    [ "isbn", "class_book_store_1_1_data_1_1books.html#ab7c54b84c6a45d6e6a45772025025cf4", null ],
    [ "pages", "class_book_store_1_1_data_1_1books.html#a530a82bdea062b2c6eaf005acec4cc86", null ],
    [ "pieces", "class_book_store_1_1_data_1_1books.html#a8d307eb4a14ca3b85d38c940b4278409", null ],
    [ "writerId", "class_book_store_1_1_data_1_1books.html#aac7ac05fbe0c092c888bff7bc2076fcb", null ]
];