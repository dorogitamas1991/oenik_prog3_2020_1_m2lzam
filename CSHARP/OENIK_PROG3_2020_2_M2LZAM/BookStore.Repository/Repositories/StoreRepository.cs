﻿// <copyright file="StoreRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookStore.Repository.Repositories
{
    using System;
    using System.Linq;
    using BookStore.Data;
    using BookStore.Repository.Interfaces;

    /// <summary>
    /// Repository for stores, which implements the IRepository with T as stores.
    /// </summary>
    public class StoreRepository : IRepository<store>
    {
        private BookDBEntities dbcontext;

        /// <summary>
        /// Initializes a new instance of the <see cref="StoreRepository"/> class.
        /// </summary>
        /// <param name="dbcontext">The database context.</param>
        public StoreRepository(BookDBEntities dbcontext)
        {
            this.dbcontext = dbcontext;
        }

        /// <summary>
        /// Adds a newly created store element to the datacontext.
        /// </summary>
        /// <param name="store">The element wanted to be added.</param>
        public void Create(store store)
        {
            var result = this.dbcontext.store.Add(store);
            this.dbcontext.SaveChanges();
        }

        /// <summary>
        /// Deletes a store element based on its id.
        /// </summary>
        /// <param name="id">The id of the store wanted to be deleted.</param>
        public void Delete(decimal id)
        {
            this.dbcontext.store.Remove(this.GetById(id));
            this.dbcontext.SaveChanges();
        }

        /// <summary>
        /// Gives back a store based on its id.
        /// </summary>
        /// <param name="id">The store's id.</param>
        /// <returns>Returns the wanted store.</returns>
        public store GetById(decimal id)
        {
            var result = this.dbcontext.store.SingleOrDefault(x => x.storeId == id);
            if (result == null)
            {
                throw new ApplicationException("Nincsen az id-nek megfelelő bolt az adatbázisban");
            }

            return result;
        }

        /// <summary>
        /// Gives back all the store table's contents.
        /// </summary>
        /// <returns>A queriable store table.</returns>
        public IQueryable<store> ReadAll()
        {
            return this.dbcontext.store;
        }

        /// <summary>
        /// Updates an existing store element.
        /// </summary>
        /// <param name="store">The updatable store element.</param>
        public void Update(store store)
        {
            var entry = this.dbcontext.Entry(this.GetById(store.storeId));
            entry.CurrentValues.SetValues(store);
            this.dbcontext.SaveChanges();
        }
    }
}
