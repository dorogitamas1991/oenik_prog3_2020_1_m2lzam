var class_book_store_1_1_repository_1_1_repositories_1_1_pieces_repository =
[
    [ "PiecesRepository", "class_book_store_1_1_repository_1_1_repositories_1_1_pieces_repository.html#a4f394b84af7d7d1f96ada7216097adbf", null ],
    [ "BookGetById", "class_book_store_1_1_repository_1_1_repositories_1_1_pieces_repository.html#a6629180b8deeca9e3ac83190dbcc9fbc", null ],
    [ "Create", "class_book_store_1_1_repository_1_1_repositories_1_1_pieces_repository.html#ac044e7c3327c5cf3432f8a766462a13b", null ],
    [ "DeleteBook", "class_book_store_1_1_repository_1_1_repositories_1_1_pieces_repository.html#a25cccfeb99c189a49a3a1c36f40ee32a", null ],
    [ "DeleteStore", "class_book_store_1_1_repository_1_1_repositories_1_1_pieces_repository.html#a1214cb81b06938c53ec6b8b9aa3eaa93", null ],
    [ "ReadAll", "class_book_store_1_1_repository_1_1_repositories_1_1_pieces_repository.html#a8708064a57bb936d161939e7e3b514a6", null ],
    [ "StoreGetById", "class_book_store_1_1_repository_1_1_repositories_1_1_pieces_repository.html#a7193dccf844af538c389a10cd319fdd7", null ],
    [ "Update", "class_book_store_1_1_repository_1_1_repositories_1_1_pieces_repository.html#abb2a19959e4eca621711e12d1d0ca66f", null ]
];