var dir_38ababae14ec831b8bac9e63b9f0bdcd =
[
    [ "BookStore", "dir_bfb3ad6a4f81363f80b0006127aac371.html", "dir_bfb3ad6a4f81363f80b0006127aac371" ],
    [ "BookStore.Data", "dir_61ed241d79fd03e1874df8d777839a91.html", "dir_61ed241d79fd03e1874df8d777839a91" ],
    [ "BookStore.Logic", "dir_4b2e189cc19d1e2fe9b163733135f24c.html", "dir_4b2e189cc19d1e2fe9b163733135f24c" ],
    [ "BookStore.Logic.Tests", "dir_1acde553231157f6a53ad30f02c4819b.html", "dir_1acde553231157f6a53ad30f02c4819b" ],
    [ "BookStore.Repository", "dir_481a638e61b1fc479e99e6a91ee4679b.html", "dir_481a638e61b1fc479e99e6a91ee4679b" ],
    [ "packages", "dir_29857c74310aa33cf297d418c86af378.html", "dir_29857c74310aa33cf297d418c86af378" ]
];