﻿// <copyright file="BooksRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookStore.Repository
{
    using System;
    using System.Linq;
    using BookStore.Data;
    using BookStore.Repository.Interfaces;

    /// <summary>
    /// Repository for books, which implements the IRepository with T as books.
    /// </summary>
    public class BooksRepository : IRepository<books>
    {
        private BookDBEntities dbcontext;

        /// <summary>
        /// Initializes a new instance of the <see cref="BooksRepository"/> class.
        /// </summary>
        /// <param name="dbcontext">The database context.</param>
        public BooksRepository(BookDBEntities dbcontext)
        {
            this.dbcontext = dbcontext;
        }

        /// <summary>
        /// Adds a new book element to the books table.
        /// </summary>
        /// <param name="book">The addable book.</param>
        public void Create(books book)
        {
            var result = this.dbcontext.books.Add(book);
            this.dbcontext.SaveChanges();
        }

        /// <summary>
        /// Deletes a book based on its id.
        /// </summary>
        /// <param name="id">The deletable book's id.</param>
        public void Delete(decimal id)
        {
            this.dbcontext.books.Remove(this.GetById(id));
            this.dbcontext.SaveChanges();
        }

        /// <summary>
        /// Gives back all the Book elements.
        /// </summary>
        /// <returns>A queryable book table.</returns>
        public IQueryable<books> ReadAll()
        {
            return this.dbcontext.books;
        }

        /// <summary>
        /// Updates an existing book.
        /// </summary>
        /// <param name="data">The updatable book.</param>
        public void Update(books data)
        {
            var entry = this.dbcontext.Entry(this.GetById(data.bookId));
            entry.CurrentValues.SetValues(data);
            this.dbcontext.SaveChanges();
        }

        /// <summary>
        /// Get a book by its id.
        /// </summary>
        /// <param name="id">The id of the wanted book.</param>
        /// <returns>The wanted book element.</returns>
        public books GetById(decimal id)
        {
            var result = this.dbcontext.books.SingleOrDefault(x => x.bookId == id);
            if (result == null)
            {
                throw new ApplicationException("Nincsen az id-nek megfelelő könyv az adatbázisban");
            }

            return result;
        }
    }
}
